enum PagedProductsType { NewArrival, BestSeller, HotDeals, ByCategory, ByBrand, ByKeyword }

enum WishListCartType { WishList, Cart }

enum CustomTextFormFieldType { Name, Phone, Email, EmailPhone, Password, NewPassword, OldPassword }

class Home {
  List<Product> newArrival;
  List<Category> topCategories;
  List<Product> bestSeller;
  List<Product> hotDeals;
  List<Category> sideMenuCategories;

  Home(
      {this.newArrival,
      this.topCategories,
      this.bestSeller,
      this.hotDeals,
      this.sideMenuCategories});

  Home.fromJson(Map<String, dynamic> json) {
    if (json['new_arrival'] != null) {
      newArrival = new List<Product>();
      json['new_arrival'].forEach((v) {
        newArrival.add(new Product.fromJson(v));
      });
    }
    if (json['top_categories'] != null) {
      topCategories = new List<Category>();
      json['top_categories'].forEach((v) {
        topCategories.add(new Category.fromJson(v));
      });
    }
    if (json['best_seller'] != null) {
      bestSeller = new List<Product>();
      json['best_seller'].forEach((v) {
        bestSeller.add(new Product.fromJson(v));
      });
    }
    if (json['hot_deals'] != null) {
      hotDeals = new List<Product>();
      json['hot_deals'].forEach((v) {
        hotDeals.add(new Product.fromJson(v));
      });
    }
    if (json['side_menu_categories'] != null) {
      sideMenuCategories = new List<Category>();
      json['side_menu_categories'].forEach((v) {
        sideMenuCategories.add(new Category.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.newArrival != null) {
      data['new_arrival'] = this.newArrival.map((v) => v.toJson()).toList();
    }
    if (this.topCategories != null) {
      data['top_categories'] =
          this.topCategories.map((v) => v.toJson()).toList();
    }
    if (this.bestSeller != null) {
      data['best_seller'] = this.bestSeller.map((v) => v.toJson()).toList();
    }
    if (this.hotDeals != null) {
      data['hot_deals'] = this.hotDeals.map((v) => v.toJson()).toList();
    }
    if (this.sideMenuCategories != null) {
      data['side_menu_categories'] =
          this.sideMenuCategories.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Product {
  int id;
  String nameAr;
  String nameEn;
  String descriptionAr;
  String descriptionEn;
  int stock;
  int status;
  String shippingSpecification;
  int subCategoryId;
  String createdAt;
  String updatedAt;
  int price;
  String longDescriptionAr;
  String longDescriptionEn;
  int brandId;
  int disable;
  String friendlyUrl;
  int countPaid;
  String url;
  var todayOffer;
  String defaultImage;
  bool isFav;
  int totalRate;
  bool isReviewed;
  List<MyColor> colors;
  List<MySize> sizes;
  List<MyImage> images;
  Subcategory subcategory;
  Brand brand;

  Product(
      {this.id,
      this.nameAr,
      this.nameEn,
      this.descriptionAr,
      this.descriptionEn,
      this.stock,
      this.status,
      this.shippingSpecification,
      this.subCategoryId,
      this.createdAt,
      this.updatedAt,
      this.price,
      this.longDescriptionAr,
      this.longDescriptionEn,
      this.brandId,
      this.disable,
      this.friendlyUrl,
      this.countPaid,
      this.url,
      this.todayOffer,
      this.defaultImage,
      this.isFav,
      this.totalRate,
      this.isReviewed,
      this.colors,
      this.sizes,
      this.images,
      this.subcategory,
      this.brand});

  Product.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nameAr = json['name_ar'];
    nameEn = json['name_en'];
    descriptionAr = json['description_ar'];
    descriptionEn = json['description_en'];
    stock = json['stock'];
    status = json['status'];
    shippingSpecification = json['shipping_specification'];
    subCategoryId = json['sub_category_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    price = json['price'];
    longDescriptionAr = json['long_description_ar'];
    longDescriptionEn = json['long_description_en'];
    brandId = json['brand_id'];
    disable = json['disable'];
    friendlyUrl = json['friendly_url'];
    countPaid = json['count_paid'];
    url = json['url'];
    todayOffer = json['today_offer'];
    defaultImage = json['default_image'];
    isFav = json['is_fav'];
    totalRate = json['total_rate'];
    isReviewed = json['is_reviewed'];
    if (json['colors'] != null) {
      colors = new List<MyColor>();
      json['colors'].forEach((v) {
        colors.add(new MyColor.fromJson(v));
      });
    }
    if (json['sizes'] != null) {
      sizes = new List<MySize>();
      json['sizes'].forEach((v) {
        sizes.add(new MySize.fromJson(v));
      });
    }
    if (json['images'] != null) {
      images = new List<MyImage>();
      json['images'].forEach((v) {
        images.add(new MyImage.fromJson(v));
      });
    }
    subcategory = json['subcategory'] != null
        ? new Subcategory.fromJson(json['subcategory'])
        : null;
    brand = json['brand'] != null ? new Brand.fromJson(json['brand']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name_ar'] = this.nameAr;
    data['name_en'] = this.nameEn;
    data['description_ar'] = this.descriptionAr;
    data['description_en'] = this.descriptionEn;
    data['stock'] = this.stock;
    data['status'] = this.status;
    data['shipping_specification'] = this.shippingSpecification;
    data['sub_category_id'] = this.subCategoryId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['price'] = this.price;
    data['long_description_ar'] = this.longDescriptionAr;
    data['long_description_en'] = this.longDescriptionEn;
    data['brand_id'] = this.brandId;
    data['disable'] = this.disable;
    data['friendly_url'] = this.friendlyUrl;
    data['count_paid'] = this.countPaid;
    data['url'] = this.url;
    data['today_offer'] = this.todayOffer;
    data['default_image'] = this.defaultImage;
    data['is_fav'] = this.isFav;
    data['total_rate'] = this.totalRate;
    data['is_reviewed'] = this.isReviewed;
    if (this.colors != null) {
      data['colors'] = this.colors.map((v) => v.toJson()).toList();
    }
    if (this.sizes != null) {
      data['sizes'] = this.sizes.map((v) => v.toJson()).toList();
    }
    if (this.images != null) {
      data['images'] = this.images.map((v) => v.toJson()).toList();
    }
    if (this.subcategory != null) {
      data['subcategory'] = this.subcategory.toJson();
    }
    if (this.brand != null) {
      data['brand'] = this.brand.toJson();
    }
    return data;
  }
}

class MyColor {
  int id;
  String color;
  int productId;
  String createdAt;
  String updatedAt;

  MyColor(
      {this.id, this.color, this.productId, this.createdAt, this.updatedAt});

  MyColor.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    color = json['color'];
    productId = json['product_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['color'] = this.color;
    data['product_id'] = this.productId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class MySize {
  int id;
  String size;
  int price;
  int productId;
  String createdAt;
  String updatedAt;

  MySize(
      {this.id,
      this.size,
      this.price,
      this.productId,
      this.createdAt,
      this.updatedAt});

  MySize.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    size = json['size'];
    price = json['price'];
    productId = json['product_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['size'] = this.size;
    data['price'] = this.price;
    data['product_id'] = this.productId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class MyImage {
  int id;
  String image;
  int productId;
  String createdAt;
  String updatedAt;

  MyImage(
      {this.id, this.image, this.productId, this.createdAt, this.updatedAt});

  MyImage.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    image = json['image'];
    productId = json['product_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['image'] = this.image;
    data['product_id'] = this.productId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class Subcategory {
  int id;
  String nameAr;
  String nameEn;
  int status;
  int categoryId;
  String createdAt;
  String updatedAt;

  Subcategory(
      {this.id,
      this.nameAr,
      this.nameEn,
      this.status,
      this.categoryId,
      this.createdAt,
      this.updatedAt});

  Subcategory.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nameAr = json['name_ar'];
    nameEn = json['name_en'];
    status = json['status'];
    categoryId = json['category_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name_ar'] = this.nameAr;
    data['name_en'] = this.nameEn;
    data['status'] = this.status;
    data['category_id'] = this.categoryId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class Category {
  int id;
  String nameAr;
  String nameEn;
  int status;
  String createdAt;
  String updatedAt;
  String image;
  int special;
  int numberOfProducts;

  Category(
      {this.id,
      this.nameAr,
      this.nameEn,
      this.status,
      this.createdAt,
      this.updatedAt,
      this.image,
      this.special,
      this.numberOfProducts});

  Category.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nameAr = json['name_ar'];
    nameEn = json['name_en'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    image = json['image'];
    special = json['special'];
    numberOfProducts = json['number_of_products'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name_ar'] = this.nameAr;
    data['name_en'] = this.nameEn;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['image'] = this.image;
    data['special'] = this.special;
    data['number_of_products'] = this.numberOfProducts;
    return data;
  }
}

class PagedProducts {
  int currentPage;
  List<Product> products;
  String firstPageUrl;
  int from;
  int lastPage;
  String lastPageUrl;
  String nextPageUrl;
  String path;
  int perPage;
  String prevPageUrl;
  int to;
  int total;

  PagedProducts(
      {this.currentPage,
      this.products,
      this.firstPageUrl,
      this.from,
      this.lastPage,
      this.lastPageUrl,
      this.nextPageUrl,
      this.path,
      this.perPage,
      this.prevPageUrl,
      this.to,
      this.total});

  PagedProducts.fromJson(Map<String, dynamic> json) {
    currentPage = json['current_page'];
    if (json['data'] != null) {
      products = new List<Product>();
      json['data'].forEach((v) {
        products.add(new Product.fromJson(v));
      });
    }
    firstPageUrl = json['first_page_url'];
    from = json['from'];
    lastPage = json['last_page'];
    lastPageUrl = json['last_page_url'];
    nextPageUrl = json['next_page_url'];
    path = json['path'];
    perPage = json['per_page'];
    prevPageUrl = json['prev_page_url'];
    to = json['to'];
    total = json['total'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['current_page'] = this.currentPage;
    if (this.products != null) {
      data['data'] = this.products.map((v) => v.toJson()).toList();
    }
    data['first_page_url'] = this.firstPageUrl;
    data['from'] = this.from;
    data['last_page'] = this.lastPage;
    data['last_page_url'] = this.lastPageUrl;
    data['next_page_url'] = this.nextPageUrl;
    data['path'] = this.path;
    data['per_page'] = this.perPage;
    data['prev_page_url'] = this.prevPageUrl;
    data['to'] = this.to;
    data['total'] = this.total;
    return data;
  }
}

class Cart {
  List<CartItem> items;
  int totalPrice;
  int totalItems;
  String shipping;

  Cart({this.items, this.totalPrice, this.totalItems, this.shipping});

  Cart.fromJson(Map<String, dynamic> json) {
    if (json['carts'] != null) {
      items = new List<CartItem>();
      json['carts'].forEach((v) {
        items.add(new CartItem.fromJson(v));
      });
    }
    totalPrice = json['total_price'];
    totalItems = json['total_items'];
    shipping = json['shipping'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.items != null) {
      data['carts'] = this.items.map((v) => v.toJson()).toList();
    }
    data['total_price'] = this.totalPrice;
    data['total_items'] = this.totalItems;
    data['shipping'] = this.shipping;
    return data;
  }
}

class CartItem {
  int id;
  int userId;
  int productId;
  int quantity;
  String createdAt;
  String updatedAt;
  Product product;

  CartItem(
      {this.id,
      this.userId,
      this.productId,
      this.quantity,
      this.createdAt,
      this.updatedAt,
      this.product});

  CartItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    productId = json['product_id'];
    quantity = json['quantity'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    product =
        json['product'] != null ? new Product.fromJson(json['product']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['product_id'] = this.productId;
    data['quantity'] = this.quantity;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.product != null) {
      data['product'] = this.product.toJson();
    }
    return data;
  }
}

class User {
  int id;
  String name;
  String email;
  String phone;
  var gender;
  String birthDate;
  int activation;
  int type;
  String image;
  String cover;
  String resetPasswordCode;
  String apiToken;
  int countryId;
  String createdAt;
  String updatedAt;
  String mobileToken;
  String os;
  String tempPhoneCode;
  int addressId;
  int socialId;
  int socialType;
  int cartCount;

  User(
      {this.id,
      this.name,
      this.email,
      this.phone,
      this.gender,
      this.birthDate,
      this.activation,
      this.type,
      this.image,
      this.cover,
      this.resetPasswordCode,
      this.apiToken,
      this.countryId,
      this.createdAt,
      this.updatedAt,
      this.mobileToken,
      this.os,
      this.tempPhoneCode,
      this.addressId,
      this.socialId,
      this.socialType,
      this.cartCount});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    phone = json['phone'];
    gender = json['gender'];
    birthDate = json['birth_date'];
    activation = json['activation'];
    type = json['type'];
    image = json['image'];
    cover = json['cover'];
    resetPasswordCode = json['reset_password_code'];
    apiToken = json['api_token'];
    countryId = json['country_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    mobileToken = json['mobile_token'];
    os = json['os'];
    tempPhoneCode = json['temp_phone_code'];
    addressId = json['address_id'];
    socialId = json['social_id'];
    socialType = json['social_type'];
    cartCount = json['cart_count'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['phone'] = this.phone;
    data['gender'] = this.gender;
    data['birth_date'] = this.birthDate;
    data['activation'] = this.activation;
    data['type'] = this.type;
    data['image'] = this.image;
    data['cover'] = this.cover;
    data['reset_password_code'] = this.resetPasswordCode;
    data['api_token'] = this.apiToken;
    data['country_id'] = this.countryId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['mobile_token'] = this.mobileToken;
    data['os'] = this.os;
    data['temp_phone_code'] = this.tempPhoneCode;
    data['address_id'] = this.addressId;
    data['social_id'] = this.socialId;
    data['social_type'] = this.socialType;
    data['cart_count'] = this.cartCount;
    return data;
  }
}

class Deals {
  List<Ads> ads;
  List<Product> hotDeals;
  List<Category> topCategories;
  List<Brand> topBrands;

  Deals({this.ads, this.hotDeals, this.topCategories, this.topBrands});

  Deals.fromJson(Map<String, dynamic> json) {
    if (json['ads'] != null) {
      ads = new List<Ads>();
      json['ads'].forEach((v) {
        ads.add(new Ads.fromJson(v));
      });
    }
    if (json['hot_deals'] != null) {
      hotDeals = new List<Product>();
      json['hot_deals'].forEach((v) {
        hotDeals.add(new Product.fromJson(v));
      });
    }
    if (json['top_categories'] != null) {
      topCategories = new List<Category>();
      json['top_categories'].forEach((v) {
        topCategories.add(new Category.fromJson(v));
      });
    }
    if (json['top_brand'] != null) {
      topBrands = new List<Brand>();
      json['top_brand'].forEach((v) {
        topBrands.add(new Brand.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.ads != null) {
      data['ads'] = this.ads.map((v) => v.toJson()).toList();
    }
    if (this.hotDeals != null) {
      data['hot_deals'] = this.hotDeals.map((v) => v.toJson()).toList();
    }
    if (this.topCategories != null) {
      data['top_categories'] =
          this.topCategories.map((v) => v.toJson()).toList();
    }
    if (this.topBrands != null) {
      data['top_brand'] = this.topBrands.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Ads {
  int id;
  String name;
  String url;
  String image;
  String createdAt;
  String updatedAt;

  Ads(
      {this.id,
      this.name,
      this.url,
      this.image,
      this.createdAt,
      this.updatedAt});

  Ads.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    url = json['url'];
    image = json['image'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['url'] = this.url;
    data['image'] = this.image;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}

class Brand {
  int id;
  String image;
  int status;
  String createdAt;
  String updatedAt;
  String nameAr;
  String nameEn;

  Brand(
      {this.id,
      this.image,
      this.status,
      this.createdAt,
      this.updatedAt,
      this.nameAr,
      this.nameEn});

  Brand.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    image = json['image'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    nameAr = json['name_ar'];
    nameEn = json['name_en'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['image'] = this.image;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['name_ar'] = this.nameAr;
    data['name_en'] = this.nameEn;
    return data;
  }
}

class OrderWithMessage {
  Order order;
  String message;
  Null url;

  OrderWithMessage({this.order, this.message, this.url});

  OrderWithMessage.fromJson(Map<String, dynamic> json) {
    order = json['data'] != null ? new Order.fromJson(json['data']) : null;
    message = json['message'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.order != null) {
      data['data'] = this.order.toJson();
    }
    data['message'] = this.message;
    data['url'] = this.url;
    return data;
  }
}

class Order {
  int id;
  int userId;
  String shippingName;
  String paymentMethod;
  String amount;
  String shipping;
  String status;
  String createdAt;
  String updatedAt;
  var expectedDeliver;
  int addressId;
  String type;
  String expectedDeliverDate;
  String humanStatus;
  String classStatus;
  int totalAmount;
  String classPayment;
  List<OrderItem> orderItems;

  Order(
      {this.id,
      this.userId,
      this.shippingName,
      this.paymentMethod,
      this.amount,
      this.shipping,
      this.status,
      this.createdAt,
      this.updatedAt,
      this.expectedDeliver,
      this.addressId,
      this.type,
      this.expectedDeliverDate,
      this.humanStatus,
      this.classStatus,
      this.totalAmount,
      this.classPayment,
      this.orderItems});

  Order.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    shippingName = json['shipping_name'];
    paymentMethod = json['payment_method'];
    amount = json['amount'];
    shipping = json['shipping'];
    status = json['status'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    expectedDeliver = json['expected_deliver'];
    addressId = json['address_id'];
    type = json['type'];
    expectedDeliverDate = json['expected_deliver_date'];
    humanStatus = json['human_status'];
    classStatus = json['class_status'];
    totalAmount = json['total_amount'];
    classPayment = json['class_payment'];
    if (json['products'] != null) {
      orderItems = new List<OrderItem>();
      json['products'].forEach((v) {
        orderItems.add(new OrderItem.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['shipping_name'] = this.shippingName;
    data['payment_method'] = this.paymentMethod;
    data['amount'] = this.amount;
    data['shipping'] = this.shipping;
    data['status'] = this.status;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['expected_deliver'] = this.expectedDeliver;
    data['address_id'] = this.addressId;
    data['type'] = this.type;
    data['expected_deliver_date'] = this.expectedDeliverDate;
    data['human_status'] = this.humanStatus;
    data['class_status'] = this.classStatus;
    data['total_amount'] = this.totalAmount;
    data['class_payment'] = this.classPayment;
    if (this.orderItems != null) {
      data['products'] = this.orderItems.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class OrderItem {
  int id;
  int orderId;
  int productId;
  String quantity;
  String price;
  String createdAt;
  String updatedAt;
  int total;
  Product product;

  OrderItem(
      {this.id,
      this.orderId,
      this.productId,
      this.quantity,
      this.price,
      this.createdAt,
      this.updatedAt,
      this.total,
      this.product});

  OrderItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    orderId = json['order_id'];
    productId = json['product_id'];
    quantity = json['quantity'];
    price = json['price'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    total = json['total'];
    product =
        json['product'] != null ? new Product.fromJson(json['product']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['order_id'] = this.orderId;
    data['product_id'] = this.productId;
    data['quantity'] = this.quantity;
    data['price'] = this.price;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['total'] = this.total;
    if (this.product != null) {
      data['product'] = this.product.toJson();
    }
    return data;
  }
}

class OrdersList {
  final List<Order> orders;

  OrdersList({
    this.orders,
  });

  factory OrdersList.fromJson(List<dynamic> parsedJson) {
    List<Order> orders = List<Order>();
    for (int i = 0; i < parsedJson.length; i++) {
      orders.add(Order.fromJson(parsedJson[i]));
    }
    return OrdersList(
      orders: orders,
    );
  }
}

class Address {
  int id;
  String city;
  String street;
  String building;
  String floor;
  String apartment;
  String phone;
  String landmark;
  String notes;
  int userId;
  String createdAt;
  String updatedAt;
  var zip;

  Address(
      {this.id,
      this.city,
      this.street,
      this.building,
      this.floor,
      this.apartment,
      this.phone,
      this.landmark,
      this.notes,
      this.userId,
      this.createdAt,
      this.updatedAt,
      this.zip});

  Address.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    city = json['city'];
    street = json['street'];
    building = json['building'];
    floor = json['floor'];
    apartment = json['apartment'];
    phone = json['phone'];
    landmark = json['landmark'];
    notes = json['notes'];
    userId = json['user_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    zip = json['zip'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['city'] = this.city;
    data['street'] = this.street;
    data['building'] = this.building;
    data['floor'] = this.floor;
    data['apartment'] = this.apartment;
    data['phone'] = this.phone;
    data['landmark'] = this.landmark;
    data['notes'] = this.notes;
    data['user_id'] = this.userId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['zip'] = this.zip;
    return data;
  }
}

class AddressesList {
  final List<Address> addresses;

  AddressesList({
    this.addresses,
  });

  factory AddressesList.fromJson(List<dynamic> parsedJson) {
    List<Address> addresses = List<Address>();
    for (int i = 0; i < parsedJson.length; i++) {
      addresses.add(Address.fromJson(parsedJson[i]));
    }
    return AddressesList(
      addresses: addresses,
    );
  }
}