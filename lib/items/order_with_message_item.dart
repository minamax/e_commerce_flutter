import 'package:flutter/material.dart';

import '../app_localization.dart';
import '../main.dart';
import '../models/model.dart';

class OrderWithMessageItem {
  static Widget build(
      BuildContext context, AsyncSnapshot<OrderWithMessage> snapshot) {
    return Padding(
      padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
      child: Card(
        elevation: 3,
        child: Container(
          padding: EdgeInsets.all(5),
          width: MediaQuery.of(context).size.width - 20,
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width / 3,
                    child: ListTile(
                      dense: true,
                      title: Center(
                        child: Text(
                          AppLocalizations.of(context).translate('ORDER ID'),
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      subtitle: Center(
                        child: Text(
                          '${snapshot.data.order.id}',
                          style: TextStyle(fontSize: 12),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width / 2.5,
                    child: ListTile(
                      dense: true,
                      title: Center(
                        child: Text(
                          AppLocalizations.of(context)
                              .translate('ORDER DATE'),
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      subtitle: Center(
                        child: Text(
                          snapshot.data.order.createdAt,
                          style: TextStyle(fontSize: 12),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                height: 85.0 * snapshot.data.order.orderItems.length,
                child: ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: snapshot.data.order.orderItems.length,
                  itemBuilder: (BuildContext context, int i) {
                    return Container(
                      width: MediaQuery.of(context).size.width - 20,
                      height: 85,
                      child: ListTile(
                        leading: Image.network(
                          'https://e-commerce-dev.intcore.net/${snapshot.data.order.orderItems[i].product.images[0].image}',
                          width: MediaQuery.of(context).size.width / 6,
                          height: 85,
                        ),
                        title: Text(
                          currentLocale.languageCode == 'en'
                              ? snapshot.data.order.orderItems[i]
                                  .product.nameEn
                              : snapshot.data.order.orderItems[i]
                                  .product.nameAr,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        subtitle: Text(
                          '\$ ${snapshot.data.order.orderItems[i].product.price}',
                          style: TextStyle(fontWeight: FontWeight.bold),
                          textAlign: TextAlign.end,
                        ),
                      ),
                    );
                  },
                ),
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text(
                    AppLocalizations.of(context).translate('Order Status') +
                        ' : ',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Text(
                    snapshot.data.order.humanStatus,
                  ),
                ],
              ),
              SizedBox(height: 15),
            ],
          ),
        ),
      ),
    );
  }
}
