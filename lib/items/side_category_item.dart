import 'package:e_commerce/main.dart';
import 'package:e_commerce/pages/paged_products_page.dart';
import 'package:flutter/material.dart';

import '../models/model.dart';

class SideCategoryItem {
  static Widget build(
      BuildContext context, List<Category> topCategories, int index) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (BuildContext context) => PagedProductsPage(
                PagedProductsType.ByCategory,
                category: topCategories[index])));
      },
      child: Column(
        children: <Widget>[
          Container(
            color: Colors.black,
            height: 55,
            child: ListTile(
              title: Text(
                currentLocale.languageCode == 'en' ?
                topCategories[index].nameEn : topCategories[index].nameAr,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                  fontSize: 18,
                ),
              ),
            ),
          ),
          Divider(
            height: 1,
            color: Colors.grey,
          ),
        ],
      ),
    );
  }
}
