import 'package:flutter/material.dart';

import '../app_localization.dart';

Widget buildAppBar(BuildContext context, String title) {
  return AppBar(
    elevation: 0.0,
    centerTitle: true,
    title: Text(AppLocalizations.of(context).translate(title),
        style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
    leading: IconButton(
      icon: Icon(Icons.arrow_back_ios),
      onPressed: () {
        Navigator.pop(context);
      },
    ),
  );
}
