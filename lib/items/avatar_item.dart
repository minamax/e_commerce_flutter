import 'package:e_commerce/services/auth_local_service.dart';
import 'package:flutter/material.dart';

import '../models/model.dart';

class AvatarItem extends StatefulWidget {
  // User user;

  @override
  _AvatarItemState createState() => _AvatarItemState();
}

class _AvatarItemState extends State<AvatarItem> {
  User user;
  bool isLoading = true;

  @override
  void initState() {
    super.initState();
    _getUser();
  }

  void _getUser() async {
    user = await AuthLocalService.instance.getUserData();
    // widget.user = user;
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return isLoading
        ? Center(
            child: CircularProgressIndicator(),
          )
        : user != null
        ? Container(
            width: MediaQuery.of(context).size.width,
            color: Colors.white,
            child: Column(
              children: <Widget>[
                CircleAvatar(
                  backgroundImage: NetworkImage(
                      'https://e-commerce-dev.intcore.net/${user.image}'),
                  radius: 50,
                ),
                SizedBox(height: 10),
                Text(user.name, style: TextStyle(fontWeight: FontWeight.bold)),
                SizedBox(height: 10),
                Text(user.email),
                SizedBox(height: 15),
              ],
            ),
          )
        : Container(
            width: MediaQuery.of(context).size.width,
            color: Colors.white,
            child: Column(
              children: <Widget>[
                CircleAvatar(
                  backgroundColor: Colors.black,
                  radius: 50,
                ),
                SizedBox(height: 10),
                Text('Hello', style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold)),
                SizedBox(height: 10),
                Text('Guest', style: TextStyle(fontSize: 16)),
                SizedBox(height: 15),
              ],
            ),
          );
  }
}
