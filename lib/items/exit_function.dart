import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../app_localization.dart';

Future<bool> exitApp(BuildContext context) {
  return showDialog(
    context: context,
    builder: (BuildContext context) => AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(10))),
          title: Text(
            AppLocalizations.of(context).translate('Exit App'),
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          content: Text(
            AppLocalizations.of(context).translate('Are You Sure ?'),
            style: TextStyle(color: Colors.red),
          ),
          actions: <Widget>[
            FlatButton(
              onPressed: () => Navigator.of(context).pop(false),
              child: Text(
                AppLocalizations.of(context).translate('No'),
                style:
                    TextStyle(color: Colors.green, fontWeight: FontWeight.bold),
              ),
            ),
            FlatButton(
              onPressed: () async {
                await SystemChannels.platform
                    .invokeMethod('SystemNavigator.pop');
              },
              child: Text(
                AppLocalizations.of(context).translate('Yes'),
                style:
                    TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
  );
}
