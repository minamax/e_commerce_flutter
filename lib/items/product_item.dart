import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';

import '../main.dart';
import '../pages/product_page.dart';
import '../models/model.dart';
import '../bloc/bloc.dart';

class ProductItem {
  static build(BuildContext context, List<Product> dataList, int index,
      GlobalKey<ScaffoldState> scaffoldKey) {
    ProgressDialog loadingDialog =
        ProgressDialog(context, ProgressDialogType.Normal);
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) =>
                    ProductPage(dataList[index])));
      },
      child: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              Card(
                color: Colors.white,
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 12.0),
                  child: FadeInImage.assetNetwork(
                    placeholder: 'assets/loading.gif',
                    image:
                        'https://e-commerce-dev.intcore.net/${dataList[index].images[0].image}',
                    fit: BoxFit.scaleDown,
                    height: MediaQuery.of(context).size.height / 5,
                    width: MediaQuery.of(context).size.width / 2,
                  ),
                ),
                margin: EdgeInsets.all(10.0),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 2,
                child: Text(
                  currentLocale.languageCode == 'en' ? dataList[index].nameEn : dataList[index].nameAr,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: 3.0),
                width: MediaQuery.of(context).size.width / 2,
                child: Text(
                  '\$ ${dataList[index].price}',
                  style: TextStyle(fontWeight: FontWeight.bold),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 1,
                ),
              ),
            ],
          ),
          StreamBuilder<Product>(
              stream: bloc.productFavoriteStream,
              builder: (BuildContext context,
                  AsyncSnapshot<Product> productSnapshot) {
                return Positioned(
                  left: MediaQuery.of(context).size.width / 2 - 40.0,
                  top: 10.0,
                  child: IconButton(
                    alignment: Alignment.center,
                    icon: dataList[index].isFav
                        ? Icon(Icons.favorite, color: Colors.red)
                        : Icon(Icons.favorite_border, color: Colors.grey),
                    onPressed: () async {
                      loadingDialog.setMessage('Please Wait...');
                      loadingDialog.show();
                      String statusMessage =
                          await bloc.favoriteProduct(dataList[index]);
                      loadingDialog.hide();
                      scaffoldKey.currentState.removeCurrentSnackBar();
                      scaffoldKey.currentState
                          .showSnackBar(SnackBar(content: Text(statusMessage)));
                    },
                  ),
                );
              }),
        ],
      ),
    );
  }
}
