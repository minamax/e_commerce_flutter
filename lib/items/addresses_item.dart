import 'package:e_commerce/main.dart';
import 'package:e_commerce/pages/edit_address_page.dart';
import 'package:flutter/material.dart';

import '../app_localization.dart';
import '../models/model.dart';

class AddressesItem extends StatefulWidget {
  final AsyncSnapshot<AddressesList> snapshot;
  final int index;

  AddressesItem([this.snapshot, this.index]);

  @override
  _AddressesItemState createState() => _AddressesItemState();
}

class _AddressesItemState extends State<AddressesItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: ListTile(
        leading: Icon(
          Icons.location_on,
          size: 40,
          color: Colors.black,
        ),
        title: Text(
          AppLocalizations.of(context).translate('Address No.') + ' ' + (widget.index + 1).toString(),
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
        subtitle: currentLocale.languageCode == 'en' ? Text(widget.snapshot.data.addresses[widget.index].building +
            ', ' +
            widget.snapshot.data.addresses[widget.index].street +
            ', ' +
            widget.snapshot.data.addresses[widget.index].city) : Text(widget.snapshot.data.addresses[widget.index].city +
            ' ,' +
            widget.snapshot.data.addresses[widget.index].street +
            ' ,' +
            widget.snapshot.data.addresses[widget.index].building),
        trailing: IconButton(
          icon: Icon(Icons.edit),
          color: Colors.black,
          onPressed: () {
            Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) => EditAddressPage(widget.snapshot, widget.index)));
          },
        ),
      ),
    );
  }
}
