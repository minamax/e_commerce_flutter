import 'package:flutter/material.dart';

import '../main.dart';
import '../pages/paged_products_page.dart';
import '../models/model.dart';

class CategoryItem {
  static Widget build(
      BuildContext context, List<Category> topCategories, int index,
      {double heightMultiplier = 1, double widthMultiplier = 1}) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (BuildContext context) => PagedProductsPage(
                PagedProductsType.ByCategory,
                category: topCategories[index])));
      },
      child: Stack(
        alignment: AlignmentDirectional.center,
        children: <Widget>[
          Card(
            elevation: 3,
            color: Colors.white,
            child: FadeInImage.assetNetwork(
              placeholder: 'assets/loading.gif',
              image:
                  'https://e-commerce-dev.intcore.net/${topCategories[index].image}',
              fit: BoxFit.scaleDown,
              height: MediaQuery.of(context).size.height / 5 * heightMultiplier,
              width: (MediaQuery.of(context).size.width - 20) * widthMultiplier,
            ),
            margin: EdgeInsets.all(10.0),
          ),
          Container(
            height: MediaQuery.of(context).size.height / 5 * heightMultiplier,
            width: (MediaQuery.of(context).size.width - 20) * widthMultiplier,
            color: Colors.black38,
          ),
          Container(
            height: MediaQuery.of(context).size.height / 5 * heightMultiplier,
            width: (MediaQuery.of(context).size.width - 20) * widthMultiplier,
            child: Center(
              child: Text(
                currentLocale.languageCode == 'en' ? topCategories[index].nameEn : topCategories[index].nameAr,
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
