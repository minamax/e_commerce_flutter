import 'package:e_commerce/main.dart';
import 'package:e_commerce/pages/product_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:progress_dialog/progress_dialog.dart';

import '../app_localization.dart';
import '../models/model.dart';
import '../bloc/bloc.dart';

class WishListCartItem {
  static Widget build(BuildContext context, AsyncSnapshot snapshot, int index,
      GlobalKey<ScaffoldState> scaffoldKey, WishListCartType type) {
    Product product;
    ProgressDialog loadingDialog =
        ProgressDialog(context, ProgressDialogType.Normal);
    switch (type) {
      case WishListCartType.WishList:
        product = snapshot.data.products[index];
        break;
      case WishListCartType.Cart:
        product = snapshot.data.items[index].product;
        break;
    }
    return Padding(
      padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
      child: InkWell(
        onTap: () {
          switch (type) {
            case WishListCartType.WishList:
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) =>
                          ProductPage(snapshot.data.products[index])));
              break;
            case WishListCartType.Cart:
              // Navigator.push(
              //     context,
              //     MaterialPageRoute(
              //         builder: (BuildContext context) =>
              //             ProductPage(snapshot.data.items[index].product)));
              break;
          }
        },
        child: Card(
          elevation: 3,
          child: Container(
            padding: EdgeInsets.all(5),
            width: MediaQuery.of(context).size.width - 20,
            height: MediaQuery.of(context).size.height / 4,
            child: Stack(
              alignment: Alignment.bottomRight,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    FadeInImage.assetNetwork(
                      placeholder: 'assets/loading.gif',
                      image:
                          'https://e-commerce-dev.intcore.net/${product.images[0].image}',
                      fit: BoxFit.scaleDown,
                      width: MediaQuery.of(context).size.width / 3,
                    ),
                    // Image.network(
                    //   'https://e-commerce-dev.intcore.net/${product.images[0].image}',
                    //   fit: BoxFit.scaleDown,
                    //   width: MediaQuery.of(context).size.width / 3,
                    // ),
                    SizedBox(width: 10),
                    Column(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(top: 3.0),
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text(
                            currentLocale.languageCode == 'en' ? product.nameEn : product.nameAr,
                            style: TextStyle(fontSize: 18),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(top: 5.0),
                          width: MediaQuery.of(context).size.width / 2,
                          alignment: AlignmentDirectional.centerEnd,
                          child: Text(
                            '\$ ${product.price}',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 16),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(bottom: 5.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      type == WishListCartType.WishList
                          ? RaisedButton(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              child: Text(
                                AppLocalizations.of(context).translate('ADD TO CART'),
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold),
                              ),
                              textColor: Colors.white,
                              onPressed: () async {
                                loadingDialog.setMessage(AppLocalizations.of(context).translate('Please Wait...'));
                                loadingDialog.show();
                                String statusMessage =
                                    await bloc.addToCart(product);
                                loadingDialog.hide();
                                scaffoldKey.currentState
                                    .removeCurrentSnackBar();
                                scaffoldKey.currentState.showSnackBar(
                                    SnackBar(content: Text(statusMessage)));
                              },
                            )
                          : Row(
                              children: <Widget>[
                                snapshot.data.items[index].quantity > 1
                                    ? IconButton(
                                        icon: Icon(Icons.remove),
                                        onPressed: () async {
                                          loadingDialog
                                              .setMessage(AppLocalizations.of(context).translate('Please Wait...'));
                                          loadingDialog.show();
                                          String statusMessage = await bloc
                                              .updateCart(product, false);
                                          loadingDialog.hide();
                                          scaffoldKey.currentState
                                              .removeCurrentSnackBar();
                                          scaffoldKey.currentState.showSnackBar(
                                              SnackBar(
                                                  content:
                                                      Text(statusMessage)));
                                        })
                                    : IconButton(
                                        icon: Icon(Icons.remove,
                                            color: Colors.grey),
                                        onPressed: () {},
                                      ),
                                Text(snapshot.data.items[index].quantity
                                    .toString()),
                                IconButton(
                                    icon: Icon(Icons.add),
                                    onPressed: () async {
                                      loadingDialog
                                          .setMessage(AppLocalizations.of(context).translate('Please Wait...'));
                                      loadingDialog.show();
                                      String statusMessage =
                                          await bloc.updateCart(product, true);
                                      loadingDialog.hide();
                                      scaffoldKey.currentState
                                          .removeCurrentSnackBar();
                                      scaffoldKey.currentState.showSnackBar(
                                          SnackBar(
                                              content: Text(statusMessage)));
                                    }),
                              ],
                            ),
                      SizedBox(width: 5),
                      IconButton(
                        icon: Icon(Icons.delete),
                        onPressed: () async {
                          String statusMessage;
                          loadingDialog.setMessage(AppLocalizations.of(context).translate('Please Wait...'));
                          loadingDialog.show();
                          switch (type) {
                            case WishListCartType.WishList:
                              statusMessage =
                                  await bloc.favoriteProduct(product);
                              break;
                            case WishListCartType.Cart:
                              statusMessage =
                                  await bloc.removeFromCart(product);
                              break;
                          }
                          loadingDialog.hide();
                          scaffoldKey.currentState.removeCurrentSnackBar();
                          scaffoldKey.currentState.showSnackBar(
                              SnackBar(content: Text(statusMessage)));
                        },
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
