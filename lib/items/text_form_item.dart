import 'package:flutter/material.dart';

import '../app_localization.dart';
import '../models/model.dart';

class CustomTextFormField extends StatefulWidget {
  final CustomTextFormFieldType type;
  final String initialValue;
  final Function onSaved;

  CustomTextFormField({
    @required this.type,
    this.initialValue = '',
    @required this.onSaved,
  });

  @override
  _CustomTextFormFieldState createState() => _CustomTextFormFieldState();
}

class _CustomTextFormFieldState extends State<CustomTextFormField> {
  String labelText;
  IconData icon;
  TextInputType keyboardType;
  bool obscureText = false;
  int maxLength;
  Function validator;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    switch (widget.type) {
      case CustomTextFormFieldType.Name:
        labelText = AppLocalizations.of(context).translate('Full Name');
        icon = Icons.person;
        keyboardType = TextInputType.text;
        obscureText = false;
        validator = (String value) {
          if (value.isEmpty || value.length < 4) {
            return AppLocalizations.of(context).translate('Invalid Name');
          } else {
            return null;
          }
        };
        break;
      case CustomTextFormFieldType.Email:
        labelText = AppLocalizations.of(context).translate('Email');
        icon = Icons.email;
        keyboardType = TextInputType.emailAddress;
        obscureText = false;
        validator = (String value) {
          if (value.isEmpty ||
              !RegExp(r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
                  .hasMatch(value)) {
            return AppLocalizations.of(context).translate('Invalid Email');
          } else {
            return null;
          }
        };
        break;
      case CustomTextFormFieldType.Phone:
        labelText = AppLocalizations.of(context).translate('Phone');
        icon = Icons.phone_android;
        keyboardType = TextInputType.number;
        obscureText = false;
        maxLength = 11;
        validator = (String value) {
          if (value.isEmpty || value.length != 11 || !value.startsWith('01')) {
            return AppLocalizations.of(context).translate('Invalid Phone');
          } else {
            return null;
          }
        };
        break;
      case CustomTextFormFieldType.Password:
        labelText = AppLocalizations.of(context).translate('Password');
        icon = Icons.lock;
        keyboardType = TextInputType.text;
        obscureText = true;
        validator = (String value) {
          if (value.isEmpty) {
            return AppLocalizations.of(context).translate('Password cant be empty');
          } else if (value.length < 6) {
            return AppLocalizations.of(context).translate('Password must be at least 6 charachters');
          } else if (!value.contains(RegExp(r'[A-Z]')) ||
              !value.contains(RegExp(r'[a-z]'))) {
            return AppLocalizations.of(context).translate('Password must include small and capital letters');
          } else {
            return null;
          }
        };
        break;
      case CustomTextFormFieldType.EmailPhone:
        labelText = AppLocalizations.of(context).translate('Email / Phone');
        icon = Icons.phone_android;
        keyboardType = TextInputType.emailAddress;
        obscureText = false;
        validator = (String value) {
          if (value.startsWith(RegExp(r'[0-9]'))) {
            bool valid = _validatePhone(value);
            if (valid)
              return null;
            else
              return AppLocalizations.of(context).translate('Invalid Phone');
          } else if (value.startsWith(RegExp(r'[a-z]'))) {
            bool valid = _validateEmail(value);
            if (valid)
              return null;
            else
              return AppLocalizations.of(context).translate('Invalid Email');
          } else if (value.isEmpty) {
            return AppLocalizations.of(context).translate('Email / Phone cant be empty');
          }
        };
        break;
      case CustomTextFormFieldType.NewPassword:
        labelText = AppLocalizations.of(context).translate('New Password');
        icon = Icons.lock;
        keyboardType = TextInputType.text;
        obscureText = true;
        validator = (String value) {
          if (value.isEmpty) {
            return AppLocalizations.of(context).translate('Password cant be empty');
          } else if (value.length < 6) {
            return AppLocalizations.of(context).translate('Password must be at least 6 charachters');
          } else if (!value.contains(RegExp(r'[A-Z]')) ||
              !value.contains(RegExp(r'[a-z]'))) {
            return AppLocalizations.of(context).translate('Password must include small and capital letters');
          } else {
            return null;
          }
        };
        break;
      case CustomTextFormFieldType.OldPassword:
        labelText = AppLocalizations.of(context).translate('Old Password');
        icon = Icons.lock_open;
        keyboardType = TextInputType.text;
        obscureText = true;
        validator = (String value) {
          if (value.isEmpty) {
            return AppLocalizations.of(context).translate('Password cant be empty');
          } else if (value.length < 6) {
            return AppLocalizations.of(context).translate('Password must be at least 6 charachters');
          } else if (!value.contains(RegExp(r'[A-Z]')) ||
              !value.contains(RegExp(r'[a-z]'))) {
            return AppLocalizations.of(context).translate('Password must include small and capital letters');
          } else {
            return null;
          }
        };
        break;
    }
    return TextFormField(
      initialValue: widget.initialValue,
      maxLength: maxLength,
      obscureText: obscureText,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey),
          borderRadius: BorderRadius.circular(10.0),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey),
          borderRadius: BorderRadius.circular(10.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey),
          borderRadius: BorderRadius.circular(10.0),
        ),
        isDense: true,
        filled: true,
        fillColor: Colors.white,
        labelText: labelText,
        labelStyle: TextStyle(color: Colors.grey),
        errorStyle: TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
        prefixIcon: Icon(
          icon,
          color: Colors.grey,
        ),
      ),
      keyboardType: keyboardType,
      validator: validator,
      onSaved: widget.onSaved,
    );
  }

  bool _validatePhone(String value) {
    if (!value.startsWith('01') || value.length != 11) {
      return false;
    } else {
      return true;
    }
  }

  bool _validateEmail(String value) {
    if (!RegExp(
            r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
        .hasMatch(value)) {
      return false;
    } else {
      return true;
    }
  }
}
