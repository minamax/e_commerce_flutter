import 'package:flutter/material.dart';

import '../app_localization.dart';
import '../models/model.dart';
import '../pages/paged_products_page.dart';

class TitleSeeMoreItem {
  static build(BuildContext context, String title, PagedProductsType pageType) {
    return Padding(
      padding: EdgeInsets.only(top: 10.0, left: 10, right: 10),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            AppLocalizations.of(context).translate(title),
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
          InkWell(
            child: Text(
              AppLocalizations.of(context).translate('See More'),
            ),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) =>
                          PagedProductsPage(pageType)));
            },
          ),
        ],
      ),
    );
  }
}
