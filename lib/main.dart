import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
// import 'package:flutter_responsive_screen/flutter_responsive_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'app_localization.dart';
import 'bloc/bloc.dart';
import 'pages/home_page.dart';
import 'pages/login_page.dart';
import 'pages/register_page.dart';

GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
SharedPreferences prefs;
Locale currentLocale = Locale('en');
// Function hp, wp;

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  void _chosenLocale() async {
    prefs = await SharedPreferences.getInstance();
    String lang = prefs.getString('language');
    if (lang != null) {
      if (lang == 'English') currentLocale = Locale('en');
      if (lang == 'Arabic') currentLocale = Locale('ar');
    }
    setState(() {});
    print("Locale is $currentLocale");
  }

  void initState() {
    super.initState();
    _chosenLocale();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<Locale>(
        stream: bloc.localeStream,
        builder: (BuildContext context, AsyncSnapshot<Locale> snapshot) {
          return MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'E-Commerce App',
            theme: ThemeData(
              brightness: Brightness.light,
              primaryColor: Colors.white,
              accentColor: Colors.black,
              buttonColor: Colors.black,
              backgroundColor: Colors.white,
              scaffoldBackgroundColor: Colors.grey[200],
            ),
            routes: {
              '/register': (BuildContext context) => RegisterPage(),
              '/login': (BuildContext context) => LoginPage(),
              '/home': (BuildContext context) => HomePage(),
            },
            navigatorKey: navigatorKey,
            supportedLocales: [
              Locale('en'),
              Locale('ar'),
            ],
            localizationsDelegates: [
              AppLocalizations.delegate,
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
            ],
            locale: snapshot.hasData ? snapshot.data : currentLocale,
            home: Loader(),
          );
        });
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }
}

class Loader extends StatefulWidget {
  @override
  _LoaderState createState() => _LoaderState();
}

class _LoaderState extends State<Loader> {
  void _loader() async {
    Future.delayed(Duration(seconds: 1))
        .then((_) => Navigator.of(context).pushReplacementNamed('/home'));
  }

  void initState() {
    super.initState();
    _loader();
  }

  @override
  Widget build(BuildContext context) {
    // wp = Screen(MediaQuery.of(context).size).wp;
    // hp = Screen(MediaQuery.of(context).size).hp;
    return Scaffold(
      body: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
