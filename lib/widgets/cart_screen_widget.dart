import 'package:e_commerce/items/wish_list_cart_item.dart';
import 'package:e_commerce/main.dart';
import 'package:e_commerce/pages/check_out_page.dart';
import 'package:flutter/material.dart';

import '../app_localization.dart';
import '../bloc/bloc.dart';
import '../models/model.dart';

class CartScreenWidget extends StatefulWidget {
  @override
  CartScreenWidgetState createState() => CartScreenWidgetState();
}

class CartScreenWidgetState extends State<CartScreenWidget> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    bloc.fetchWishListCart(WishListCartType.Cart);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.grey[200],
      body: StreamBuilder<Cart>(
        stream: bloc.cartStream,
        builder: (BuildContext context, AsyncSnapshot<Cart> snapshot) {
          if (snapshot.hasData) {
            return snapshot.data.items.isNotEmpty
                ? _buildCart(snapshot)
                : Center(
                    child: Text(
                      AppLocalizations.of(context).translate('No Items'),
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                  );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }

  Widget _buildCart(AsyncSnapshot<Cart> snapshot) {
    if (snapshot.data.items.length == 0) {
      return Center(
          child: Text(AppLocalizations.of(context).translate('No Items'),
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)));
    } else {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: ListView.builder(
              itemCount: snapshot.data.items.length,
              itemBuilder: (BuildContext context, int index) {
                return WishListCartItem.build(context, snapshot, index,
                    _scaffoldKey, WishListCartType.Cart);
              },
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(left: 40, right: 40),
                child: buildSubTotal(context, snapshot),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 50,
                child: RaisedButton(
                  child: Text(
                    AppLocalizations.of(context).translate('CHECK OUT'),
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                  ),
                  textColor: Colors.white,
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) =>
                            CheckOutPage(snapshot)));
                  },
                ),
              ),
            ],
          ),
        ],
      );
    }
  }

  Widget buildSubTotal(BuildContext context, AsyncSnapshot<Cart> snapshot) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 10),
          child: Text(
            AppLocalizations.of(context).translate('SUBTOTAL'),
            style: TextStyle(
                fontSize: 18, fontWeight: FontWeight.bold, color: Colors.grey),
          ),
        ),
        Padding(
          padding: currentLocale.languageCode == 'en'
              ? EdgeInsets.only(top: 10, right: 20)
              : EdgeInsets.only(top: 10, left: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                snapshot.data.totalItems.toString() +
                    ' ' +
                    AppLocalizations.of(context).translate('Items'),
                style: TextStyle(fontSize: 16, color: Colors.black),
              ),
              Text(
                '\$ ${snapshot.data.totalPrice}',
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
            ],
          ),
        ),
        Padding(
          padding: currentLocale.languageCode == 'en'
              ? EdgeInsets.only(top: 10, right: 20)
              : EdgeInsets.only(top: 10, left: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                AppLocalizations.of(context).translate('Shipping'),
                style: TextStyle(fontSize: 16, color: Colors.black),
              ),
              SizedBox(width: 150),
              Text(
                '\$ ${snapshot.data.shipping}',
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
            ],
          ),
        ),
        Divider(height: 20),
        Padding(
          padding: currentLocale.languageCode == 'en'
              ? EdgeInsets.only(bottom: 15, left: 10, right: 25)
              : EdgeInsets.only(bottom: 15, left: 25, right: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                AppLocalizations.of(context).translate('TOTAL'),
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
              Text(
                '\$ ${snapshot.data.totalPrice + int.parse(snapshot.data.shipping)}',
                style: TextStyle(
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                    color: Colors.black),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
