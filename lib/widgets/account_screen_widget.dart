import 'package:e_commerce/models/model.dart';
import 'package:e_commerce/pages/about_page.dart';
import 'package:e_commerce/pages/contact_page.dart';
import 'package:e_commerce/pages/language_page.dart';
import 'package:e_commerce/services/auth_local_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../app_localization.dart';
import '../pages/edit_user_page.dart';
import '../pages/home_page.dart';
import '../pages/orders_page.dart';
import '../pages/addresses_page.dart';
import '../pages/change_password_page.dart';
import '../items/avatar_item.dart';

class AccountScreenWidget extends StatefulWidget {
  @override
  _AccountScreenWidgetState createState() => _AccountScreenWidgetState();
}

class _AccountScreenWidgetState extends State<AccountScreenWidget> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  User user;
  AvatarItem avatar = AvatarItem();
  bool isLoading = true;
  bool switchValue = true;

  @override
  void initState() {
    super.initState();
    _getUser();
  }

  void _getUser() async {
    user = await AuthLocalService.instance.getUserData();
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.grey[200],
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    return isLoading
        ? Center(
            child: CircularProgressIndicator(),
          )
        : user == null
            ? SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    avatar,
                    SizedBox(height: 10),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height / 8,
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Text(
                          AppLocalizations.of(context).translate('Login'),
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                        textColor: Colors.white,
                        onPressed: () {
                          Navigator.of(context).pushReplacementNamed('/login');
                        },
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height / 8,
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Text(
                          AppLocalizations.of(context).translate('Register'),
                          style: TextStyle(
                              fontSize: 18, fontWeight: FontWeight.bold),
                        ),
                        textColor: Colors.white,
                        onPressed: () {
                          Navigator.of(context)
                              .pushReplacementNamed('/register');
                        },
                      ),
                    ),
                  ],
                ),
              )
            : SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Stack(
                      alignment: Alignment.topRight,
                      children: <Widget>[
                        AvatarItem(),
                        IconButton(
                          icon: Icon(Icons.edit),
                          onPressed: () {
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    EditUserPage(user),
                              ),
                            );
                          },
                        ),
                        Positioned(
                          left: 5,
                          child: IconButton(
                            icon: Icon(Icons.exit_to_app,
                                size: 30, color: Colors.black),
                            onPressed: () async {
                              User user =
                                  await AuthLocalService.instance.getUserData();
                              if (user != null) {
                                SharedPreferences prefs =
                                    await SharedPreferences.getInstance();
                                bool status = await prefs.remove('user');
                                if (status == true)
                                  Navigator.of(context).pushNamedAndRemoveUntil(
                                      '/home', (Route<dynamic> route) => false);
                              } else {
                                _scaffoldKey.currentState.showSnackBar(SnackBar(
                                  content: Text(AppLocalizations.of(context)
                                      .translate(
                                          'You Are Already in Guest Mode')),
                                ));
                              }
                            },
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 5),
                    _buildPageItem(
                        AppLocalizations.of(context).translate('My Orders'),
                        Icons.drive_eta,
                        OrdersPage()),
                    _buildPageItem(
                        AppLocalizations.of(context).translate('My Addresses'),
                        Icons.location_on,
                        AddressesPage()),
                    _buildPageItem(
                        AppLocalizations.of(context).translate('My Wishlist'),
                        Icons.favorite,
                        HomePage(2)),
                    _buildPageItem(
                        AppLocalizations.of(context).translate('My Cart'),
                        Icons.shopping_cart,
                        HomePage(3)),
                    SizedBox(height: 5),
                    Container(
                      color: Colors.white,
                      child: SwitchListTile(
                        dense: true,
                        value: switchValue,
                        title: Text(
                          AppLocalizations.of(context).translate('Push Notifications'),
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        onChanged: (bool newValue) {
                          setState(() {
                            switchValue = newValue;
                          });
                        },
                      ),
                    ),
                    Divider(height: 2),
                    _buildListItem(
                        AppLocalizations.of(context).translate('Change Password'), ChangePasswordPage(user.apiToken)),
                    _buildListItem(AppLocalizations.of(context).translate('Language'), LanguagePage()),
                    _buildListItem(AppLocalizations.of(context).translate('Contact Us'), ContactPage()),
                    _buildListItem(AppLocalizations.of(context).translate('About'), AboutPage()),
                    SizedBox(height: 5),
                  ],
                ),
              );
  }

  Widget _buildPageItem(String title, IconData icon, StatefulWidget widget) {
    return Column(
      children: <Widget>[
        Container(
          color: Colors.white,
          child: ListTile(
            title: Text(
              title,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            leading: Icon(icon),
            trailing: Icon(Icons.arrow_forward_ios),
            onTap: () {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => widget));
            },
          ),
        ),
        Divider(height: 3),
      ],
    );
  }

  Widget _buildListItem(String title, StatefulWidget widget) {
    return Column(
      children: <Widget>[
        Container(
          color: Colors.white,
          child: ListTile(
            dense: true,
            title: Text(
              title,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            trailing: Icon(Icons.arrow_forward_ios),
            onTap: () {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (BuildContext context) => widget));
            },
          ),
        ),
        Divider(height: 2),
      ],
    );
  }
}