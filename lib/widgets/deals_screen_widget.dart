import 'package:e_commerce/items/brand_item.dart';
import 'package:e_commerce/pages/categories_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../app_localization.dart';
import '../bloc/bloc.dart';
import '../models/model.dart';
import '../pages/brands_page.dart';
import '../items/category_item.dart';
import '../items/product_item.dart';
import '../items/title_seemore_item.dart';

class DealsScreenWidget extends StatefulWidget {
  @override
  _DealsScreenWidgetState createState() => _DealsScreenWidgetState();
}

class _DealsScreenWidgetState extends State<DealsScreenWidget> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    bloc.fetchDeals();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.grey[200],
      body: StreamBuilder<Deals>(
        stream: bloc.dealsStream,
        builder: (BuildContext context, AsyncSnapshot<Deals> snapshot) {
          if (snapshot.hasData) {
            return SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  snapshot.data.ads.isNotEmpty
                      ? _buildAdsList(snapshot)
                      : Container(),
                  snapshot.data.hotDeals.isNotEmpty
                      ? _buildHotDeals(snapshot)
                      : Container(),
                  snapshot.data.topCategories.isNotEmpty
                      ? _buildTopList(snapshot, PagedProductsType.ByCategory)
                      : Container(),
                  snapshot.data.topBrands.isNotEmpty
                      ? _buildTopList(snapshot, PagedProductsType.ByBrand)
                      : Container(),
                ],
              ),
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }

  Widget _buildAdsList(AsyncSnapshot<Deals> snapshot) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height / 3,
          width: MediaQuery.of(context).size.width,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: snapshot.data.ads.length,
            itemBuilder: (BuildContext context, int index) {
              return Card(
                elevation: 3,
                color: Colors.white,
                child: Image.network(
                  'https://e-commerce-dev.intcore.net/${snapshot.data.ads[index].image}',
                  fit: BoxFit.cover,
                  height: MediaQuery.of(context).size.height / 5,
                  width: MediaQuery.of(context).size.width - 20,
                ),
                margin: EdgeInsets.all(10.0),
              );
            },
          ),
        ),
        Divider(),
      ],
    );
  }

  Widget _buildHotDeals(AsyncSnapshot<Deals> snapshot) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        TitleSeeMoreItem.build(
            context, 'HOT DEALS', PagedProductsType.HotDeals),
        Container(
          height: MediaQuery.of(context).size.height / 3,
          width: MediaQuery.of(context).size.width,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: snapshot.data.hotDeals.length,
            itemBuilder: (BuildContext context, int index) {
              return ProductItem.build(
                  context, snapshot.data.hotDeals, index, _scaffoldKey);
            },
          ),
        ),
        Divider(),
      ],
    );
  }

  Widget _buildTopList(
      AsyncSnapshot<Deals> snapshot, PagedProductsType pageType) {
    String _title;
    List _dataList;
    switch (pageType) {
      case PagedProductsType.ByCategory:
        _title = 'TOP CATEGORIES';
        _dataList = snapshot.data.topCategories;
        break;
      case PagedProductsType.ByBrand:
        _title = 'TOP BRAND';
        _dataList = snapshot.data.topBrands;
        break;
      default:
        break;
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 5.0, left: 10, right: 10),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                AppLocalizations.of(context).translate(_title),
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              InkWell(
                child: Text(AppLocalizations.of(context).translate('See More')),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => pageType == PagedProductsType.ByCategory ?
                              CategoriesPage(): BrandsPage()));
                },
              ),
            ],
          ),
        ),
        Container(
          height: MediaQuery.of(context).size.height / 4,
          width: MediaQuery.of(context).size.width,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: _dataList.length,
            itemBuilder: (BuildContext context, int index) {
              switch (pageType) {
                case PagedProductsType.ByCategory:
                  return CategoryItem.build(
                      context, _dataList.cast<Category>(), index, widthMultiplier: .8);
                  break;
                case PagedProductsType.ByBrand:
                  return BrandItem.build(
                      context, _dataList.cast<Brand>(), index, widthMultiplier: .8);
                  break;
                default:
                  break;
              }
            },
          ),
        ),
      ],
    );
  }
}
