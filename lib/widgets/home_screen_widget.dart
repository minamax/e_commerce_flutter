import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../app_localization.dart';
import '../bloc/bloc.dart';
import '../models/model.dart';
import '../pages/categories_page.dart';
import '../items/category_item.dart';
import '../items/product_item.dart';
import '../items/title_seemore_item.dart';

class HomeScreenWidget extends StatefulWidget {
  @override
  _HomeScreenWidgetState createState() => _HomeScreenWidgetState();
}

class _HomeScreenWidgetState extends State<HomeScreenWidget> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    bloc.fetchHome();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.grey[200],
      body: StreamBuilder<Home>(
        stream: bloc.homeStream,
        builder: (BuildContext context, AsyncSnapshot<Home> snapshot) {
          if (snapshot.hasData) {
            return SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  snapshot.data.newArrival.isNotEmpty
                      ? _buildHorizontalList(
                          snapshot, PagedProductsType.NewArrival)
                      : Container(),
                  snapshot.data.bestSeller.isNotEmpty
                      ? _buildHorizontalList(
                          snapshot, PagedProductsType.BestSeller)
                      : Container(),
                  snapshot.data.hotDeals.isNotEmpty
                      ? _buildHorizontalList(
                          snapshot, PagedProductsType.HotDeals)
                      : Container(),
                  snapshot.data.topCategories.isNotEmpty
                      ? _buildTopCategories(snapshot)
                      : Container(),
                ],
              ),
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }

  Widget _buildHorizontalList(
      AsyncSnapshot<Home> snapshot, PagedProductsType pageType) {
    String _title;
    List<Product> _dataList;
    switch (pageType) {
      case PagedProductsType.NewArrival:
        _title = AppLocalizations.of(context).translate('NEW ARRIVAL');
        _dataList = snapshot.data.newArrival;
        break;
      case PagedProductsType.BestSeller:
        _title = AppLocalizations.of(context).translate('BEST SELLER');
        _dataList = snapshot.data.bestSeller;
        break;
      case PagedProductsType.HotDeals:
        _title = AppLocalizations.of(context).translate('HOT DEALS');
        _dataList = snapshot.data.hotDeals;
        break;
      case PagedProductsType.ByCategory:
        break;
      case PagedProductsType.ByBrand:
        break;
      case PagedProductsType.ByKeyword:
        break;
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        TitleSeeMoreItem.build(context, _title, pageType),
        Container(
          height: MediaQuery.of(context).size.height / 3,
          width: MediaQuery.of(context).size.width,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: _dataList.length,
            itemBuilder: (BuildContext context, int index) {
              return ProductItem.build(context, _dataList, index, _scaffoldKey);
            },
          ),
        ),
        Divider(),
      ],
    );
  }

  Widget _buildTopCategories(AsyncSnapshot<Home> snapshot) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 10.0, left: 10, right: 10),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                AppLocalizations.of(context).translate('TOP CATEGORIES'),
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              InkWell(
                child: Text(
                  AppLocalizations.of(context).translate('See More'),
                ),
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => CategoriesPage()));
                },
              ),
            ],
          ),
        ),
        Container(
          height: MediaQuery.of(context).size.height / 4,
          width: MediaQuery.of(context).size.width,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: snapshot.data.topCategories.length,
            itemBuilder: (BuildContext context, int index) {
              return CategoryItem.build(
                  context, snapshot.data.topCategories, index,
                  widthMultiplier: .8);
            },
          ),
        ),
      ],
    );
  }
}
