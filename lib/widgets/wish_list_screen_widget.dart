import 'package:e_commerce/items/wish_list_cart_item.dart';
import 'package:flutter/material.dart';

import '../app_localization.dart';
import '../bloc/bloc.dart';
import '../models/model.dart';

class WishListScreenWidget extends StatefulWidget {
  @override
  _WishListScreenWidgetState createState() => _WishListScreenWidgetState();
}

class _WishListScreenWidgetState extends State<WishListScreenWidget> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    bloc.fetchWishListCart(WishListCartType.WishList);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.grey[200],
      body: StreamBuilder<PagedProducts>(
        stream: bloc.wishListStream,
        builder: (BuildContext context, AsyncSnapshot<PagedProducts> snapshot) {
          if (snapshot.hasData) {
            return snapshot.data.products.isNotEmpty
                ? _buildWishList(snapshot)
                : Center(
                    child: Text(
                      AppLocalizations.of(context).translate('No Items'),
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                  );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }

  Widget _buildWishList(AsyncSnapshot<PagedProducts> snapshot) {
    return StreamBuilder<Product>(
        stream: bloc.productFavoriteStream,
        builder:
            (BuildContext context, AsyncSnapshot<Product> productSnapshot) {
          if (snapshot.data.products.length == 0) {
            return Center(
                child: Text(AppLocalizations.of(context).translate('No Items'),
                    style:
                        TextStyle(fontSize: 20, fontWeight: FontWeight.bold)));
          } else {
            return ListView.builder(
              itemCount: snapshot.data.products.length,
              itemBuilder: (BuildContext context, int index) {
                return WishListCartItem.build(context, snapshot, index,
                    _scaffoldKey, WishListCartType.WishList);
              },
            );
          }
        });
  }
}
