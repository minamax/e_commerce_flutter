import 'dart:convert';
import 'dart:ui';

import 'package:e_commerce/main.dart';
import 'package:e_commerce/network/auth_api_service.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/model.dart';
import '../network/home_api_service.dart';
import '../network/paged_products_api_service.dart';
import '../network/favorite_api_service.dart';
import '../network/cart_api_service.dart';
import '../network/deals_api_service.dart';
import '../network/orders_api_service.dart';
import '../network/addresses_api_service.dart';

class Bloc {
  Home home;
  Deals deals;
  Cart cart;
  OrdersList orders;
  OrderWithMessage createdOrder;
  User user;
  AddressesList addresses;
  PagedProducts newArrival;
  PagedProducts bestSeller;
  PagedProducts hotDeals;
  PagedProducts wishList;
  PagedProducts categoryData;
  PagedProducts brandData;
  PagedProducts searchData;

  final _localeFetcher = PublishSubject<Locale>();
  final _homeFetcher = PublishSubject<Home>();
  final _dealsFetcher = PublishSubject<Deals>();
  final _cartFetcher = PublishSubject<Cart>();
  final _ordersFetcher = PublishSubject<OrdersList>();
  final _createdOrderFetcher = PublishSubject<OrderWithMessage>();
  final _userFetcher = PublishSubject<User>();
  final _addressesFetcher = PublishSubject<AddressesList>();
  final _productFavorite = PublishSubject<Product>();
  final _newArrivalFetcher = PublishSubject<PagedProducts>();
  final _bestSellerFetcher = PublishSubject<PagedProducts>();
  final _hotDealsFetcher = PublishSubject<PagedProducts>();
  final _wishListFetcher = PublishSubject<PagedProducts>();
  final _categoryDataFetcher = PublishSubject<PagedProducts>();
  final _brandDataFetcher = PublishSubject<PagedProducts>();
  final _searchDataFetcher = PublishSubject<PagedProducts>();

  Observable<Locale> get localeStream => _localeFetcher.stream;
  Observable<Home> get homeStream => _homeFetcher.stream;
  Observable<Deals> get dealsStream => _dealsFetcher.stream;
  Observable<Cart> get cartStream => _cartFetcher.stream;
  Observable<OrdersList> get ordersStream => _ordersFetcher.stream;
  Observable<OrderWithMessage> get createdOrderStream =>
      _createdOrderFetcher.stream;
  Observable<User> get userStream => _userFetcher.stream;
  Observable<AddressesList> get addressesStream => _addressesFetcher.stream;
  Observable<Product> get productFavoriteStream => _productFavorite.stream;
  Observable<PagedProducts> get newArrivalStream => _newArrivalFetcher.stream;
  Observable<PagedProducts> get bestSellerStream => _bestSellerFetcher.stream;
  Observable<PagedProducts> get hotDealsStream => _hotDealsFetcher.stream;
  Observable<PagedProducts> get wishListStream => _wishListFetcher.stream;
  Observable<PagedProducts> get categoryStream => _categoryDataFetcher.stream;
  Observable<PagedProducts> get brandStream => _brandDataFetcher.stream;
  Observable<PagedProducts> get searchStream => _searchDataFetcher.stream;

  void setLocale(String language) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('language', language);
    if (language == 'English') {
      currentLocale = Locale('en');
    }
    if (language == 'Arabic') {
      currentLocale = Locale('ar');
    }
    _localeFetcher.sink.add(currentLocale);
  }

  void fetchHome() async {
    home = await HomeApiService.instance.fetchHome();
    _homeFetcher.sink.add(home);
  }

  void fetchDeals() async {
    deals = await DealsApiService.instance.fetchDeals();
    _dealsFetcher.sink.add(deals);
  }

  void fetchOrders() async {
    orders = await OrdersApiService.instance.fetchOrders();
    _ordersFetcher.sink.add(orders);
  }

  void createOrder(int addressId, int paymentMethod) async {
    createdOrder =
        await OrdersApiService.instance.createOrder(addressId, paymentMethod);
    _createdOrderFetcher.sink.add(createdOrder);
  }

  void fetchAddresses() async {
    addresses = await AddressesApiService.instance.fetchAddresses();
    _addressesFetcher.sink.add(addresses);
  }

  void fetchUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    dynamic userData = json.decode(prefs.getString('user'));
    user =
        await AuthApiService.instance.getProfile(userData['user']['api_token']);
    _userFetcher.sink.add(user);
  }

  void fetchPagedProducts(PagedProductsType type,
      {Category category, Brand brand, String keyword}) async {
    switch (type) {
      case PagedProductsType.NewArrival:
        {
          newArrival =
              await PagedProductsApiService.instance.fetchPagedProducts(type);
          _newArrivalFetcher.sink.add(newArrival);
        }
        break;
      case PagedProductsType.BestSeller:
        {
          bestSeller =
              await PagedProductsApiService.instance.fetchPagedProducts(type);
          _bestSellerFetcher.sink.add(bestSeller);
        }
        break;
      case PagedProductsType.HotDeals:
        {
          hotDeals =
              await PagedProductsApiService.instance.fetchPagedProducts(type);
          _hotDealsFetcher.sink.add(hotDeals);
        }
        break;
      case PagedProductsType.ByCategory:
        {
          _categoryDataFetcher.drain();
          categoryData = await PagedProductsApiService.instance
              .fetchPagedProducts(type, category: category);
          _categoryDataFetcher.sink.add(categoryData);
        }
        break;
      case PagedProductsType.ByBrand:
        {
          _brandDataFetcher.drain();
          brandData = await PagedProductsApiService.instance
              .fetchPagedProducts(type, brand: brand);
          _brandDataFetcher.sink.add(brandData);
        }
        break;
      case PagedProductsType.ByKeyword:
        {
          _searchDataFetcher.drain();
          searchData = await PagedProductsApiService.instance
              .fetchPagedProducts(type, keyword: keyword);
          _searchDataFetcher.sink.add(searchData);
        }
        break;
    }
  }

  Future<bool> fetchPagedProductsNextPage(PagedProductsType type) async {
    switch (type) {
      case PagedProductsType.NewArrival:
        if (newArrival.currentPage < newArrival.lastPage) {
          PagedProducts newPage = await PagedProductsApiService.instance
              .fetchPagedProductsNextPage(type, newArrival.nextPageUrl);
          newArrival.currentPage = newPage.currentPage;
          newArrival.nextPageUrl = newPage.nextPageUrl;
          newArrival.products.addAll(newPage.products);
          _newArrivalFetcher.drain();
          _newArrivalFetcher.sink.add(newArrival);
          return true;
        } else {
          return false;
        }
        break;
      case PagedProductsType.BestSeller:
        if (bestSeller.currentPage < bestSeller.lastPage) {
          PagedProducts newPage = await PagedProductsApiService.instance
              .fetchPagedProductsNextPage(type, bestSeller.nextPageUrl);
          bestSeller.currentPage = newPage.currentPage;
          bestSeller.nextPageUrl = newPage.nextPageUrl;
          bestSeller.products.addAll(newPage.products);
          _bestSellerFetcher.drain();
          _bestSellerFetcher.sink.add(bestSeller);
          return true;
        } else {
          return false;
        }
        break;
      case PagedProductsType.HotDeals:
        if (hotDeals.currentPage < hotDeals.lastPage) {
          PagedProducts newPage = await PagedProductsApiService.instance
              .fetchPagedProductsNextPage(type, hotDeals.nextPageUrl);
          hotDeals.currentPage = newPage.currentPage;
          hotDeals.nextPageUrl = newPage.nextPageUrl;
          hotDeals.products.addAll(newPage.products);
          _hotDealsFetcher.drain();
          _hotDealsFetcher.sink.add(hotDeals);
          return true;
        } else {
          return false;
        }
        break;
      case PagedProductsType.ByCategory:
        if (categoryData.currentPage < categoryData.lastPage) {
          PagedProducts newPage = await PagedProductsApiService.instance
              .fetchPagedProductsNextPage(type, categoryData.nextPageUrl);
          categoryData.currentPage = newPage.currentPage;
          categoryData.nextPageUrl = newPage.nextPageUrl;
          categoryData.products.addAll(newPage.products);
          _categoryDataFetcher.drain();
          _categoryDataFetcher.sink.add(categoryData);
          return true;
        } else {
          return false;
        }
        break;
      case PagedProductsType.ByBrand:
        if (brandData.currentPage < brandData.lastPage) {
          PagedProducts newPage = await PagedProductsApiService.instance
              .fetchPagedProductsNextPage(type, brandData.nextPageUrl);
          brandData.currentPage = newPage.currentPage;
          brandData.nextPageUrl = newPage.nextPageUrl;
          brandData.products.addAll(newPage.products);
          _brandDataFetcher.drain();
          _brandDataFetcher.sink.add(brandData);
          return true;
        } else {
          return false;
        }
        break;
      case PagedProductsType.ByKeyword:
        if (searchData.currentPage < searchData.lastPage) {
          PagedProducts newPage = await PagedProductsApiService.instance
              .fetchPagedProductsNextPage(type, searchData.nextPageUrl);
          searchData.currentPage = newPage.currentPage;
          searchData.nextPageUrl = newPage.nextPageUrl;
          searchData.products.addAll(newPage.products);
          _searchDataFetcher.drain();
          _searchDataFetcher.sink.add(searchData);
          return true;
        } else {
          return false;
        }
        break;
    }

    return false;
  }

  Future<String> favoriteProduct(Product product) async {
    _productFavorite.drain();
    String statusMessage =
        await FavoriteApiService.instance.favoriteProduct(product);
    _productFavorite.sink.add(product);
    return statusMessage;
  }

  Future<String> addToCart(Product product) async {
    String statusMessage = await CartApiService.instance.addToCart(product);
    cart = await CartApiService.instance.fetchCart();
    _cartFetcher.sink.add(cart);
    return statusMessage;
  }

  Future<String> removeFromCart(Product product) async {
    for (int i = 0; i < cart.items.length; i++) {
      if (cart.items[i].product.id == product.id) {
        String statusMessage =
            await CartApiService.instance.removeFromCart(cart.items[i]);
        cart.totalPrice -= cart.items[i].quantity * cart.items[i].product.price;
        cart.totalItems -= 1;
        cart.items.removeAt(i);
        _cartFetcher.sink.add(cart);
        return statusMessage;
      }
    }
    return 'Item Is Not In Cart';
  }

  Future<String> updateCart(Product product, bool isIncrement) async {
    for (int i = 0; i < cart.items.length; i++) {
      if (cart.items[i].product.id == product.id) {
        String statusMessage = await CartApiService.instance
            .updateCartItem(cart.items[i], isIncrement);
        if (isIncrement) {
          cart.items[i].quantity++;
          cart.totalPrice += cart.items[i].product.price;
        } else {
          cart.items[i].quantity--;
          cart.totalPrice -= cart.items[i].product.price;
        }
        _cartFetcher.sink.add(cart);
        return statusMessage;
      }
    }
    return 'Item Is Not In Cart';
  }

  void fetchWishListCart(WishListCartType type) async {
    switch (type) {
      case WishListCartType.WishList:
        wishList = await FavoriteApiService.instance.fetchWishList();
        _wishListFetcher.sink.add(wishList);
        break;
      case WishListCartType.Cart:
        cart = await CartApiService.instance.fetchCart();
        _cartFetcher.sink.add(cart);
        break;
    }
  }

  void dispose() {
    _localeFetcher.close();
    _homeFetcher.close();
    _dealsFetcher.close();
    _cartFetcher.close();
    _ordersFetcher.close();
    _createdOrderFetcher.close();
    _userFetcher.close();
    _addressesFetcher.close();
    _productFavorite.close();
    _newArrivalFetcher.close();
    _bestSellerFetcher.close();
    _hotDealsFetcher.close();
    _wishListFetcher.close();
    _categoryDataFetcher.close();
    _searchDataFetcher.close();
  }
}

final Bloc bloc = Bloc();
