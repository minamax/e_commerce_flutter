import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

import '../models/model.dart';

class AuthLocalService {
  static AuthLocalService instance = AuthLocalService();

  Future<bool> storeUserData(dynamic responseData) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('user', json.encode(responseData));
    return true;
  }

  Future<User> getUserData() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getString('user') != null) {
      dynamic userData = json.decode(prefs.getString('user'));
      print(userData);
      User user = User.fromJson(userData['user']);
      print(user.toJson());
      return user;
    } else {
      return null;
    }
  }

  Future<bool> storeCode(String code) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('code', code);
    return true;
  }

  Future<bool> verifyCode(String code) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String savedCode = prefs.getString('code');
    if (savedCode == code)
      return true;
    else
      return false;
  }
}
