import '../models/model.dart';

class OrdersLocalService {
  static OrdersLocalService instance = OrdersLocalService();

  Future<OrdersList> decodeOrders(List<dynamic> responseData) async {
    OrdersList orders = OrdersList.fromJson(responseData);
    return orders;
  }

  Future<OrderWithMessage> decodeCreatedOrder(dynamic responseData) async {
    OrderWithMessage order = OrderWithMessage.fromJson(responseData);
    return order;
  }
}
