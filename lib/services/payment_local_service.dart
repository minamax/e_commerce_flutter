import 'dart:convert';

import 'package:crypto/crypto.dart';

class PaymentLocalService {
  static PaymentLocalService instance = PaymentLocalService();

  Future<String> calculateSignature(
      Map<String, dynamic> tokenRequestParameters) async {
    var sortedParameters = tokenRequestParameters.keys.toList()..sort();
    String signature = '';
    for (String key in sortedParameters) {
      signature += key + '=' + tokenRequestParameters[key];
    }
    signature = 'TESTSHAIN' + signature + 'TESTSHAIN';    
    var bytes = utf8.encode(signature);
    var digits = sha256.convert(bytes);
    signature = digits.toString();
    return signature;
  }
}
