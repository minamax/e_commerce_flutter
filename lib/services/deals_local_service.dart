import '../models/model.dart';

class DealsLocalService {
  static DealsLocalService instance = DealsLocalService();

  Future<Deals> decodeDeals(dynamic responseData) async {
    Deals deals = Deals.fromJson(responseData);
    return deals;
  }
}
