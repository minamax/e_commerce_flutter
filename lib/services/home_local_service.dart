import '../models/model.dart';

class HomeLocalService {
  static HomeLocalService instance = HomeLocalService();

  Future<Home> decodeHome(dynamic responseData) async {
    Home home = Home.fromJson(responseData);
    return home;
  }
}
