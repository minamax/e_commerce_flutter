import '../models/model.dart';

class AddressesLocalService {
  static AddressesLocalService instance = AddressesLocalService();

  Future<AddressesList> decodeAddresses(List<dynamic> responseData) async {
    AddressesList addresses = AddressesList.fromJson(responseData);
    return addresses;
  }
}
