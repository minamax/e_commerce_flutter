import '../models/model.dart';
import '../bloc/bloc.dart';

class FavoriteLocalService {
  static FavoriteLocalService instance = FavoriteLocalService();

  Future<bool> favoriteProduct(Product product) async {
    for (int i = 0; i < bloc.home.newArrival.length; i++) {
      if (bloc.home.newArrival[i].id == product.id) {
        bloc.home.newArrival[i].isFav = !bloc.home.newArrival[i].isFav;
      }
    }
    for (int i = 0; i < bloc.home.bestSeller.length; i++) {
      if (bloc.home.bestSeller[i].id == product.id) {
        bloc.home.bestSeller[i].isFav = !bloc.home.bestSeller[i].isFav;
      }
    }
    for (int i = 0; i < bloc.home.hotDeals.length; i++) {
      if (bloc.home.hotDeals[i].id == product.id) {
        bloc.home.hotDeals[i].isFav = !bloc.home.hotDeals[i].isFav;
      }
    }
    if (bloc.deals != null) {
      for (int i = 0; i < bloc.deals.hotDeals.length; i++) {
        if (bloc.deals.hotDeals[i].id == product.id) {
          bloc..deals.hotDeals[i].isFav =
              !bloc.deals.hotDeals[i].isFav;
        }
      }
    }
    if (bloc.newArrival != null) {
      for (int i = 0; i < bloc.newArrival.products.length; i++) {
        if (bloc.newArrival.products[i].id == product.id) {
          bloc.newArrival.products[i].isFav =
              !bloc.newArrival.products[i].isFav;
        }
      }
    }
    if (bloc.hotDeals != null) {
      for (int i = 0; i < bloc.hotDeals.products.length; i++) {
        if (bloc.hotDeals.products[i].id == product.id) {
          bloc.hotDeals.products[i].isFav =
              !bloc.hotDeals.products[i].isFav;
        }
      }
    }
    if (bloc.bestSeller != null) {
      for (int i = 0; i < bloc.bestSeller.products.length; i++) {
        if (bloc.bestSeller.products[i].id == product.id) {
          bloc.bestSeller.products[i].isFav =
              !bloc.bestSeller.products[i].isFav;
        }
      }
    }
    if (bloc.categoryData != null) {
      for (int i = 0; i < bloc.categoryData.products.length; i++) {
        if (bloc.categoryData.products[i].id == product.id) {
          bloc.categoryData.products[i].isFav =
              !bloc.categoryData.products[i].isFav;
        }
      }
    }
    if (bloc.brandData != null) {
      for (int i = 0; i < bloc.brandData.products.length; i++) {
        if (bloc.brandData.products[i].id == product.id) {
          bloc.brandData.products[i].isFav =
              !bloc.brandData.products[i].isFav;
        }
      }
    }
    if (bloc.searchData != null) {
      for (int i = 0; i < bloc.searchData.products.length; i++) {
        if (bloc.searchData.products[i].id == product.id) {
          bloc.searchData.products[i].isFav =
              !bloc.searchData.products[i].isFav;
        }
      }
    }
    if (bloc.wishList != null) {
      for (int i = 0; i < bloc.wishList.products.length; i++) {
        if (bloc.wishList.products[i].id == product.id) {
          bloc.wishList.products.removeAt(i);
        }
      }
    }
    return true;
  }

  Future<PagedProducts> decodeWishList(dynamic responseData) async {
    PagedProducts wishList = PagedProducts.fromJson(responseData);
    return wishList;
  }
}
