import '../models/model.dart';

class PagedProductsLocalService {
  static PagedProductsLocalService instance = PagedProductsLocalService();

  Future<PagedProducts> decodePagedProducts(
      PagedProductsType type, dynamic responseData) async {
    PagedProducts pagedProducts;
    if (type == PagedProductsType.ByCategory ||
        type == PagedProductsType.ByBrand ||
        type == PagedProductsType.ByKeyword) {
      pagedProducts = PagedProducts.fromJson(responseData['products']);
    } else {
      pagedProducts = PagedProducts.fromJson(responseData);
    }
    return pagedProducts;
  }
}
