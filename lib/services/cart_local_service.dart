import '../models/model.dart';

class CartLocalService {
  static CartLocalService instance = CartLocalService();

  Future<Cart> decodeCart(dynamic responseData) async {
    Cart cart = Cart.fromJson(responseData);
    return cart;
  }
}
