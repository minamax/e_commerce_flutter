import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:http_parser/http_parser.dart';

import '../main.dart';
import '../services/auth_local_service.dart';
import '../models/model.dart';

class AuthApiService {
  static AuthApiService instance = AuthApiService();

  Future<String> loginUser(Map<String, dynamic> formData) async {
    http.Response response = await http.post(
        'https://e-commerce-dev.intcore.net/api/v1/user/auth/signin?locale=${currentLocale.languageCode}',
        body: formData,
        headers: {
          "Content-type": "application/x-www-form-urlencoded",
          "Accept": "application/json"
        });
    final responseData = json.decode(response.body);
    print(responseData);
    if (response.statusCode == 200) {
      await AuthLocalService.instance.storeUserData(responseData);
      return 'ok';
    } else {
      return responseData['errors'][0]["message"];
    }
  }

  Future<String> registerUser(Map<String, dynamic> formData) async {
    http.Response response = await http.post(
        'https://e-commerce-dev.intcore.net/api/v1/user/auth/signup?locale=${currentLocale.languageCode}',
        body: formData,
        headers: {
          "Content-type": "application/x-www-form-urlencoded",
          "Accept": "application/json"
        });
    final responseData = json.decode(response.body);
    print(responseData);
    if (response.statusCode == 200) {
      await AuthLocalService.instance.storeUserData(responseData);
      return 'ok';
    } else {
      return responseData['errors'][0]["message"];
    }
  }

  Future<String> resetEmail(String phone) async {
    http.Response response = await http.post(
        'https://e-commerce-dev.intcore.net/api/v1/user/auth/send-reset-password-code?locale=${currentLocale.languageCode}',
        body: {
          "phone": phone,
        },
        headers: {
          "Content-type": "application/x-www-form-urlencoded",
          "Accept": "application/json"
        });
    final responseData = json.decode(response.body);
    print(response.statusCode);
    if (response.body.contains('code')) {
      await AuthLocalService.instance
          .storeCode(responseData['code'].toString());
      return responseData['code'].toString();
    } else {
      return responseData['errors'][0]["message"];
    }
  }

  Future<String> resetPassword(
      String phone, String code, String newPassword) async {
    http.Response response = await http.patch(
        'https://e-commerce-dev.intcore.net/api/v1/user/auth/reset-password?phone=$phone&reset_password_code=$code&password=$newPassword&locale=${currentLocale.languageCode}',
        headers: {
          "Content-type": "application/x-www-form-urlencoded",
          "Accept": "application/json"
        });
    final responseData = json.decode(response.body);
    print(response.body);
    if (response.statusCode == 200) {
      await AuthLocalService.instance.storeUserData(responseData);
      return 'Password Changed Successfully';
    } else {
      return responseData['phone'];
    }
  }

  Future<String> updatePassword(
      String apiToken, String oldPassword, String newPassword) async {
    http.Response response = await http.patch(
        'https://e-commerce-dev.intcore.net/api/v1/user/auth/update-password?api_token=$apiToken&old_password=$oldPassword&new_password=$newPassword&locale=${currentLocale.languageCode}',
        headers: {
          "Content-type": "application/x-www-form-urlencoded",
          "Accept": "application/json"
        });
    final responseData = json.decode(response.body);
    print(response.body);
    if (response.statusCode == 200) {
      return 'Password Changed Successfully';
    } else {
      return responseData['errors'][0]['message'];
    }
  }

  Future<String> updateProfile(
      Map<String, dynamic> formData, bool hasImage) async {
    if (hasImage) {
      Map<String, String> data = {
        'api_token': formData['api_token'].toString(),
        'name': formData['name'].toString(),
        'email': formData['email'].toString(),
        'gender': formData['gender'].toString(),
        'birth_date': formData['birth_date'].toString(),
      };
      var url = Uri.parse(
          'https://e-commerce-dev.intcore.net/api/v1/user/auth/update-profile');
      var request = http.MultipartRequest("POST", url);
      request.fields.addAll(data);
      var file = await http.MultipartFile.fromPath(
        'image',
        formData['image'],
        contentType: MediaType('image', 'jpg'),
      );
      request.files.add(file);
      StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        await AuthApiService.instance.getProfile(formData['api_token']);
        return 'Profile Updated Successfully';
      } else {
        return 'Image Upload Failed';
      }
    } else {
      http.Response response = await http.post(
          'https://e-commerce-dev.intcore.net/api/v1/user/auth/update-profile?locale=${currentLocale.languageCode}',
          body: {
            'api_token': formData['api_token'].toString(),
            'name': formData['name'].toString(),
            'email': formData['email'].toString(),
            'gender': formData['gender'].toString(),
            'birth_date': formData['birth_date'].toString(),
          },
          headers: {
            "Content-type": "application/x-www-form-urlencoded",
            "Accept": "application/json"
          });
      final responseData = json.decode(response.body);
      print(response.body);
      if (response.body.contains('user')) {
        await AuthLocalService.instance.storeUserData(responseData);
        return 'Profile Updated Successfully';
      } else {
        return responseData['errors'][0]['message'];
      }
    }
  }

  Future<User> getProfile(String apiToken) async {
    http.Response response = await http.get(
        'https://e-commerce-dev.intcore.net/api/v1/user/auth/get-profile?api_token=$apiToken&locale=${currentLocale.languageCode}',
        headers: {
          "Content-type": "application/x-www-form-urlencoded",
          "Accept": "application/json"
        });
    final responseData = json.decode(response.body);
    print(responseData);
    if (response.statusCode == 200) {
      AuthLocalService.instance.storeUserData(responseData);
      return User.fromJson(responseData);
    } else {
      return responseData['errors'][0]["message"];
    }
  }
}
