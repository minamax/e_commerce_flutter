import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../services/deals_local_service.dart';
import '../models/model.dart';

class DealsApiService {
  static DealsApiService instance = DealsApiService();

  Future<Deals> fetchDeals() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String string = prefs.getString('user');
    dynamic userData;
    if (string != null) {
      userData = json.decode(string);
    }
    http.Response response;
    if (userData != null) {
      print(userData);
      response = await http.get(
          'https://e-commerce-dev.intcore.net/api/v1/user/app/deals-page?api_token=${userData['user']['api_token']}',
          headers: {
            "Content-type": "application/x-www-form-urlencoded",
            "Accept": "application/json"
          });
    } else {
      response = await http.get(
          'https://e-commerce-dev.intcore.net/api/v1/user/app/deals-page',
          headers: {
            "Content-type": "application/x-www-form-urlencoded",
            "Accept": "application/json"
          });
    }
    final responseData = json.decode(response.body);
    print(responseData);
    if (response.statusCode == 200) {
      Deals decodedDeals =
          await DealsLocalService.instance.decodeDeals(responseData);
      return decodedDeals;
    } else {
      print(responseData['errors'][0]["message"]);
      return null;
    }
  }
}
