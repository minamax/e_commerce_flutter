import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';
import '../services/addresses_local_service.dart';
import '../models/model.dart';

class AddressesApiService {
  static AddressesApiService instance = AddressesApiService();

  Future<AddressesList> fetchAddresses() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String string = prefs.getString('user');
    dynamic userData;
    if (string != null) {
      userData = json.decode(string);
    }
    if (userData == null) {
      return null;
    } else {
      print(userData);
      http.Response response = await http.get(
          'https://e-commerce-dev.intcore.net/api/v1/user/app/address?api_token=${userData['user']['api_token']}',
          headers: {
            "Content-type": "application/x-www-form-urlencoded",
            "Accept": "application/json"
          });
      if (response.statusCode == 200) {
        List<dynamic> responseData = json.decode(response.body);
        print(responseData);
        AddressesList decodedAddresses =
            await AddressesLocalService.instance.decodeAddresses(responseData);
        return decodedAddresses;
      } else {
        dynamic responseData = json.decode(response.body);
        print(responseData);
        print(responseData['errors'][0]["message"]);
        return null;
      }
    }
  }

  Future<String> updateAddress(Address address) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String string = prefs.getString('user');
    dynamic userData;
    if (string != null) {
      userData = json.decode(string);
    }
    if (userData == null) {
      return null;
    } else {
      http.Response response = await http.patch(
          'https://e-commerce-dev.intcore.net/api/v1/user/app/address/${address.id}?city=${address.city}&street=${address.street}&building=${address.building}&floor=${address.floor}&apartment=${address.apartment}&landmark=${address.landmark}&phone=${address.phone}&notes=${address.notes}&api_token=${userData['user']['api_token']}&locale=${currentLocale.languageCode}',
          headers: {
            "Content-type": "application/x-www-form-urlencoded",
            "Accept": "application/json"
          });
      dynamic responseData = json.decode(response.body);
      if (response.statusCode == 200) {
        return 'Address Updated Successfully';
      } else {
        return responseData['errors'][0]["message"];
      }
    }
  }

  Future<String> createAddress(Address address) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String string = prefs.getString('user');
    dynamic userData;
    if (string != null) {
      userData = json.decode(string);
    }
    if (userData == null) {
      return null;
    } else {
      http.Response response = await http.post(
          'https://e-commerce-dev.intcore.net/api/v1/user/app/address?locale=${currentLocale.languageCode}',
          body: {
            'city': address.city,
            'street': address.street,
            'building': address.building,
            'floor': address.floor,
            'apartment': address.apartment,
            'landmark': address.landmark,
            'phone': address.phone,
            'notes': address.notes,
            'api_token': userData['user']['api_token'].toString(),
          },
          headers: {
            "Content-type": "application/x-www-form-urlencoded",
            "Accept": "application/json"
          });
      dynamic responseData = json.decode(response.body);
      if (response.statusCode == 200) {
        return 'Address Created Successfully';
      } else {
        return responseData['errors'][0]["message"];
      }
    }
  }
}
