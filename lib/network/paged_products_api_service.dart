import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../services/paged_products_local_service.dart';
import '../models/model.dart';

class PagedProductsApiService {
  static PagedProductsApiService instance = PagedProductsApiService();

  Future<PagedProducts> fetchPagedProducts(PagedProductsType type,
      {Category category, Brand brand, String keyword}) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String string = prefs.getString('user');
    dynamic userData;
    if (string != null) {
      userData = json.decode(string);
    }
    String listType;
    switch (type) {
      case PagedProductsType.NewArrival:
        listType = 'new-arrival?';
        break;
      case PagedProductsType.BestSeller:
        listType = 'best-seller?';
        break;
      case PagedProductsType.HotDeals:
        listType = 'hot-deals?';
        break;
      case PagedProductsType.ByCategory:
        listType = 'products?category_id=${category.id}&';
        print(category.id);
        break;
      case PagedProductsType.ByBrand:
        listType = 'products?brand_id=${brand.id}&';
        print(brand.id);
        break;
      case PagedProductsType.ByKeyword:
        listType = 'products?keyword=$keyword&';
        print(keyword);
        break;
    }
    http.Response response;
    if (userData != null) {
      print(userData);
      response = await http.get(
          'https://e-commerce-dev.intcore.net/api/v1/user/app/${listType}api_token=${userData['user']['api_token']}',
          headers: {
            "Content-type": "application/x-www-form-urlencoded",
            "Accept": "application/json"
          });
    } else {
      response = await http.get(
          'https://e-commerce-dev.intcore.net/api/v1/user/app/$listType',
          headers: {
            "Content-type": "application/x-www-form-urlencoded",
            "Accept": "application/json"
          });
    }
    final responseData = json.decode(response.body);
    print(responseData);
    if (response.statusCode == 200) {
      PagedProducts decodedPagedProducts = await PagedProductsLocalService
          .instance
          .decodePagedProducts(type, responseData);
      return decodedPagedProducts;
    } else {
      print(responseData['errors'][0]["message"]);
      return null;
    }
  }

  Future<PagedProducts> fetchPagedProductsNextPage(
      PagedProductsType type, String nextPageUrl) async {
    http.Response response = await http.get(nextPageUrl, headers: {
      "Content-type": "application/x-www-form-urlencoded",
      "Accept": "application/json"
    });
    final responseData = json.decode(response.body);
    print(responseData);
    if (response.statusCode == 200) {
      PagedProducts decodedNewPage = await PagedProductsLocalService.instance
          .decodePagedProducts(type, responseData);
      return decodedNewPage;
    } else {
      print(responseData['errors'][0]["message"]);
      return null;
    }
  }
}
