import 'dart:convert';

import 'package:http/http.dart' as http;

import '../main.dart';
import '../services/payment_local_service.dart';

class PaymentApiService {
  static PaymentApiService instance = PaymentApiService();

  Future<dynamic> requestToken(String deviceId) async {
    Map<String, dynamic> tokenRequestParameters = {
      'service_command': 'SDK_TOKEN',
      'access_code': '3396N0aO6L5WUGNzwKyl',
      'merchant_identifier': 'PwWMJFAJ',
      'language': currentLocale.languageCode,
      'device_id': deviceId,
    };
    String signature = await PaymentLocalService.instance
        .calculateSignature(tokenRequestParameters);
    tokenRequestParameters['signature'] = signature;
    print(tokenRequestParameters);
    String body = json.encode(tokenRequestParameters);
    http.Response response = await http.post(
        'https://sbpaymentservices.payfort.com/FortAPI/paymentApi',
        body: body,
        headers: {"Accept": "application/json"});
    final responseData = json.decode(response.body);
    return responseData;
  }
}
