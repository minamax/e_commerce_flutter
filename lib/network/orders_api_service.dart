import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../services/orders_local_service.dart';
import '../models/model.dart';

class OrdersApiService {
  static OrdersApiService instance = OrdersApiService();

  Future<OrdersList> fetchOrders() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String string = prefs.getString('user');
    dynamic userData;
    if (string != null) {
      userData = json.decode(string);
    }
    if (userData == null) {
      return null;
    } else {
      print(userData);
      http.Response response = await http.get(
          'https://e-commerce-dev.intcore.net/api/v1/user/app/order?api_token=${userData['user']['api_token']}',
          headers: {
            "Content-type": "application/x-www-form-urlencoded",
            "Accept": "application/json"
          });
      if (response.statusCode == 200) {
        List<dynamic> responseData = json.decode(response.body);
        print(responseData);
        OrdersList decodedOrders =
            await OrdersLocalService.instance.decodeOrders(responseData);
        return decodedOrders;
      } else {
        dynamic responseData = json.decode(response.body);
        print(responseData);
        print(responseData['errors'][0]["message"]);
        return null;
      }
    }
  }

  Future<OrderWithMessage> createOrder(int addressId, int paymentMethod) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String string = prefs.getString('user');
    dynamic userData;
    if (string != null) {
      userData = json.decode(string);
    }
    if (userData == null) {
      return null;
    } else {
      print(userData);
      http.Response response = await http.post(
          'https://e-commerce-dev.intcore.net/api/v1/user/app/order',
          body: {
            'api_token': userData['user']['api_token'].toString(),
            'address_id': addressId.toString(),
            'payment_method': paymentMethod.toString(),
          },
          headers: {
            "Content-type": "application/x-www-form-urlencoded",
            "Accept": "application/json"
          });
      if (response.statusCode == 200) {
        dynamic responseData = json.decode(response.body);
        print(responseData);
        OrderWithMessage decodedOrder =
            await OrdersLocalService.instance.decodeCreatedOrder(responseData);
        return decodedOrder;
      } else {
        dynamic responseData = json.decode(response.body);
        print(responseData);
        print(responseData['errors'][0]["message"]);
        return null;
      }
    }
  }
}
