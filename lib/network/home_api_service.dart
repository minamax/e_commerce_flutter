import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../services/home_local_service.dart';
import '../models/model.dart';

class HomeApiService {
  static HomeApiService instance = HomeApiService();

  Future<Home> fetchHome() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String string = prefs.getString('user');
    dynamic userData;
    if (string != null) {
      userData = json.decode(string);
    }
    http.Response response;
    if (userData != null) {
      print(userData);
      response = await http.get(
          'https://e-commerce-dev.intcore.net/api/v1/user/app/home?api_token=${userData['user']['api_token']}',
          headers: {
            "Content-type": "application/x-www-form-urlencoded",
            "Accept": "application/json"
          });
    } else {
      response = await http.get(
          'https://e-commerce-dev.intcore.net/api/v1/user/app/home',
          headers: {
            "Content-type": "application/x-www-form-urlencoded",
            "Accept": "application/json"
          });
    }
    final responseData = json.decode(response.body);
    print(responseData);
    if (response.statusCode == 200) {
      Home decodedHome =
          await HomeLocalService.instance.decodeHome(responseData);
      return decodedHome;
    } else {
      print(responseData['errors'][0]["message"]);
      return null;
    }
  }

  Future<Map<String, dynamic>> fetchAbout() async {
    http.Response response = await http.get(
        'https://e-commerce-dev.intcore.net/api/v1/user/app/about',
        headers: {
          "Content-type": "application/x-www-form-urlencoded",
          "Accept": "application/json"
        });
    final Map<String, dynamic> responseData = json.decode(response.body);
    print(responseData);
    return responseData;
  }

  Future<String> contact(Map<String, dynamic> formData) async {
    http.Response response = await http.post(
        'https://e-commerce-dev.intcore.net/api/v1/user/app/contact-us',
        body: {
          'name': formData['name'],
          'email': formData['email'],
          'message': formData['message'],
        },
        headers: {
          "Content-type": "application/x-www-form-urlencoded",
          "Accept": "application/json"
        });
    if (response.statusCode == 200) {
      return 'Message Sent Successfully';
    } else
      return 'Message Sending Failed';
  }
}
