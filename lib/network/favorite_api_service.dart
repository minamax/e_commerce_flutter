import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';
import '../services/favorite_local_service.dart';
import '../models/model.dart';

class FavoriteApiService {
  static FavoriteApiService instance = FavoriteApiService();

  Future<String> favoriteProduct(Product product) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String string = prefs.getString('user');
    dynamic userData;
    if (string != null) {
      userData = json.decode(string);
    }
    if (userData == null) {
      Timer(Duration(seconds: 2), () {
        navigatorKey.currentState.pushNamed('/login');
      });
      return 'Please Login First';
    } else {
      print(userData);
      http.Response response = await http.post(
          'https://e-commerce-dev.intcore.net/api/v1/user/app/action/favourite?locale=${currentLocale.languageCode}',
          body: {
            'api_token': userData['user']['api_token'],
            'product_id': product.id.toString(),
          },
          headers: {
            "Content-type": "application/x-www-form-urlencoded",
            "Accept": "application/json"
          });
      final responseData = json.decode(response.body);
      print(responseData);
      if (response.statusCode == 200) {
        bool status =
            await FavoriteLocalService.instance.favoriteProduct(product);
        if (status == true) {
          return responseData['message'];
        } else {
          return 'Local Error';
        }
      } else {
        return responseData['errors'][0]["message"];
      }
    }
  }

  Future<PagedProducts> fetchWishList() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String string = prefs.getString('user');
    dynamic userData;
    if (string != null) {
      userData = json.decode(string);
    }
    if (userData == null) {
      PagedProducts temp = PagedProducts(
          currentPage: 0,
          products: [],
          firstPageUrl: '',
          from: 0,
          to: 0,
          total: 0,
          lastPage: 0,
          perPage: 0,
          lastPageUrl: '',
          nextPageUrl: '',
          prevPageUrl: '',
          path: '');
      return temp;
    } else {
      print(userData);
      http.Response response = await http.get(
          'https://e-commerce-dev.intcore.net/api/v1/user/auth/favourite?api_token=${userData['user']['api_token']}',
          headers: {
            "Content-type": "application/x-www-form-urlencoded",
            "Accept": "application/json"
          });
      final responseData = json.decode(response.body);
      print(responseData);
      if (response.statusCode == 200) {
        PagedProducts decodedWishList =
            await FavoriteLocalService.instance.decodeWishList(responseData);
        return decodedWishList;
      } else {
        print(responseData['errors'][0]["message"]);
        return null;
      }
    }
  }

  Future<String> rateProduct(Product product, String review, int rating) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String string = prefs.getString('user');
    dynamic userData;
    if (string != null) {
      userData = json.decode(string);
    }
    if (userData == null) {
      return 'Please Login First';
    } else {
      print(userData);
      http.Response response = await http.post(
          'https://e-commerce-dev.intcore.net/api/v1/user/app/action/review?locale=${currentLocale.languageCode}',
          body: {
            'api_token': userData['user']['api_token'],
            'product_id': product.id.toString(),
            'review': review,
            'rate': '${rating + 1}',
          },
          headers: {
            "Content-type": "application/x-www-form-urlencoded",
            "Accept": "application/json"
          });
      final responseData = json.decode(response.body);
      print(responseData);
      if (response.statusCode == 200) {
        return 'Product Rated Successfully';
      } else {
        return responseData['errors'][0]["message"];
      }
    }
  }
}
