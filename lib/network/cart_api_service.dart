import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';
import '../services/cart_local_service.dart';
import '../models/model.dart';

class CartApiService {
  static CartApiService instance = CartApiService();

  Future<String> addToCart(Product product) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String string = prefs.getString('user');
    dynamic userData;
    if (string != null) {
      userData = json.decode(string);
    }
    if (userData == null) {
      Timer(Duration(seconds: 2), () {
        navigatorKey.currentState.pushNamed('/login');
      });
      return 'Please Login First';
    } else {
      print(userData);
      http.Response response = await http.post(
          'https://e-commerce-dev.intcore.net/api/v1/user/app/cart/add?locale=${currentLocale.languageCode}',
          body: {
            'api_token': userData['user']['api_token'],
            'product_id': product.id.toString(),
            'quantity': '1',
          },
          headers: {
            "Content-type": "application/x-www-form-urlencoded",
            "Accept": "application/json"
          });
      final responseData = json.decode(response.body);
      print(responseData);
      if (response.body.contains('error')) {
        return responseData['errors'][0]["message"];
      } else {
        return 'Item Added Successfully';
      }
    }
  }

  Future<String> updateCartItem(CartItem cartItem, bool isIncrement) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String string = prefs.getString('user');
    dynamic userData;
    if (string != null) {
      userData = json.decode(string);
    }
    if (userData == null) {
      navigatorKey.currentState.pushNamed('/login');
      return 'Please Login First';
    } else {
      print(userData);
      int quantity;
      if (isIncrement) {
        quantity = 1;
      } else {
        quantity = 0;
      }
      http.Response response = await http.patch(
          'https://e-commerce-dev.intcore.net/api/v1/user/app/cart/update-cart?api_token=${userData['user']['api_token']}&quantity=$quantity&cart_id=${cartItem.id}&locale=${currentLocale.languageCode}',
          headers: {
            "Content-type": "application/x-www-form-urlencoded",
            "Accept": "application/json"
          });
      final responseData = json.decode(response.body);
      print(responseData);
      if (response.body.contains('error')) {
        return responseData['errors'][0]["message"];
      } else {
        return 'Quantity Updated Successfully';
      }
    }
  }

  Future<String> removeFromCart(CartItem cartItem) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String string = prefs.getString('user');
    dynamic userData;
    if (string != null) {
      userData = json.decode(string);
    }
    if (userData == null) {
      navigatorKey.currentState.pushNamed('/login');
      return 'Please Login First';
    } else {
      print(userData);
      http.Response response = await http.delete(
          'https://e-commerce-dev.intcore.net/api/v1/user/app/cart/delete/${cartItem.id}?api_token=${userData['user']['api_token']}&locale=${currentLocale.languageCode}',
          headers: {
            "Content-type": "application/x-www-form-urlencoded",
            "Accept": "application/json"
          });
      final responseData = json.decode(response.body);
      print(responseData);
      if (response.body.contains('error')) {
        return responseData['errors'][0]["message"];
      } else {
        return 'Item Removed Successfully';
      }
    }
  }

  Future<Cart> fetchCart() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String string = prefs.getString('user');
    dynamic userData;
    if (string != null) {
      userData = json.decode(string);
    }
    if (userData == null) {
      Cart temp = Cart(items: [], totalItems: 0, totalPrice: 0, shipping: "0");
      return temp;
    } else {
      print(userData);
      http.Response response = await http.get(
          'https://e-commerce-dev.intcore.net/api/v1/user/app/cart/get-carts?api_token=${userData['user']['api_token']}&locale=${currentLocale.languageCode}',
          headers: {
            "Content-type": "application/x-www-form-urlencoded",
            "Accept": "application/json"
          });
      final responseData = json.decode(response.body);
      print(responseData);
      if (response.statusCode == 200) {
        Cart decodedCart =
            await CartLocalService.instance.decodeCart(responseData);
        return decodedCart;
      } else {
        print(responseData['errors'][0]["message"]);
        return null;
      }
    }
  }
}
