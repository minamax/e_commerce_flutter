import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';

import '../app_localization.dart';
import '../models/model.dart';
import '../network/addresses_api_service.dart';
import 'addresses_page.dart';

class EditAddressPage extends StatefulWidget {
  final AsyncSnapshot<AddressesList> snapshot;
  final int index;
  EditAddressPage([this.snapshot, this.index]);

  @override
  _EditAddressPageState createState() => _EditAddressPageState();
}

class _EditAddressPageState extends State<EditAddressPage> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String city, street, building, floor, apartment, phone, landmark, notes;
  ProgressDialog loadingDialog;
  bool isEdit = false;

  @override
  void initState() {
    super.initState();
    if (widget.snapshot != null && widget.index != null) {
      isEdit = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      key: _scaffoldKey,
      appBar: _buildAppBar(context, isEdit),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(20),
            child: Column(
              children: <Widget>[
                _buildTextField(
                  labelText: AppLocalizations.of(context).translate('City'),
                  initialValue: isEdit
                      ? widget.snapshot.data.addresses[widget.index].city
                      : null,
                  onSaved: (String value) {
                    city = value;
                  },
                  isRequired: true,
                ),
                SizedBox(height: 10),
                _buildTextField(
                  labelText: AppLocalizations.of(context).translate('Street'),
                  initialValue: isEdit
                      ? widget.snapshot.data.addresses[widget.index].street
                      : null,
                  onSaved: (String value) {
                    street = value;
                  },
                  isRequired: true,
                ),
                SizedBox(height: 10),
                _buildTextField(
                  labelText: AppLocalizations.of(context).translate('Building'),
                  initialValue: isEdit
                      ? widget.snapshot.data.addresses[widget.index].building
                      : null,
                  onSaved: (String value) {
                    building = value;
                  },
                  isRequired: true,
                ),
                SizedBox(height: 10),
                _buildTextField(
                  labelText: AppLocalizations.of(context).translate('Floor'),
                  initialValue: isEdit
                      ? widget.snapshot.data.addresses[widget.index].floor
                      : null,
                  onSaved: (String value) {
                    floor = value;
                  },
                ),
                SizedBox(height: 10),
                _buildTextField(
                  labelText: AppLocalizations.of(context).translate('Apartment'),
                  initialValue: isEdit
                      ? widget.snapshot.data.addresses[widget.index].apartment
                      : null,
                  onSaved: (String value) {
                    apartment = value;
                  },
                ),
                SizedBox(height: 10),
                _buildTextField(
                  labelText: AppLocalizations.of(context).translate('Phone'),
                  initialValue: isEdit
                      ? widget.snapshot.data.addresses[widget.index].phone
                      : null,
                  onSaved: (String value) {
                    phone = value;
                  },
                  isRequired: true,
                ),
                SizedBox(height: 10),
                _buildTextField(
                  labelText: AppLocalizations.of(context).translate('Landmark'),
                  initialValue: isEdit
                      ? widget.snapshot.data.addresses[widget.index].landmark
                      : null,
                  onSaved: (String value) {
                    landmark = value;
                  },
                ),
                SizedBox(height: 10),
                _buildTextField(
                  labelText: AppLocalizations.of(context).translate('Notes'),
                  initialValue: isEdit
                      ? widget.snapshot.data.addresses[widget.index].notes
                      : null,
                  onSaved: (String value) {
                    notes = value;
                  },
                ),
                SizedBox(height: 10),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildAppBar(BuildContext context, bool isEdit) {
    return AppBar(
      elevation: 0.0,
      leading: IconButton(
        icon: Icon(Icons.done),
        onPressed: () {
          _submitForm(isEdit);
        },
      ),
      title: Center(
        child: Text(
          isEdit ? AppLocalizations.of(context).translate('Edit Address') : AppLocalizations.of(context).translate('Add Address'),
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
      ),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.close),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ],
    );
  }

  void _submitForm(isEdit) async {
    // showDialog<Null>(
    //     context: context,
    //     barrierDismissible: false,
    //     builder: (BuildContext context) {
    //       return Container(
    //         width: 50,
    //         height: 50,
    //         child: Dialog(
    //           child: Container(
    //             child: CircularProgressIndicator(),
    //           ),
    //         ),
    //       );
    //     });
    FocusScope.of(context).unfocus();
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      Address newAddress = Address(
        city: city,
        apartment: apartment,
        building: building,
        floor: floor,
        notes: notes,
        landmark: landmark,
        phone: phone,
        street: street,
        id: isEdit ? widget.snapshot.data.addresses[widget.index].id : null,
        userId:
            isEdit ? widget.snapshot.data.addresses[widget.index].userId : null,
        createdAt: isEdit
            ? widget.snapshot.data.addresses[widget.index].createdAt
            : null,
        updatedAt: isEdit
            ? widget.snapshot.data.addresses[widget.index].updatedAt
            : null,
        zip: isEdit ? widget.snapshot.data.addresses[widget.index].zip : null,
      );
      loadingDialog = ProgressDialog(context, ProgressDialogType.Normal);
      loadingDialog.setMessage(AppLocalizations.of(context).translate('Please Wait...'));
      loadingDialog.show();
      String status;
      if (isEdit) {
        status = await AddressesApiService.instance.updateAddress(newAddress);
      } else {
        status = await AddressesApiService.instance.createAddress(newAddress);
      }
      // Navigator.pop(context);
      loadingDialog.hide();
      if (status == 'Address Updated Successfully' ||
          status == 'Address Created Successfully') {
        Future.delayed(Duration(seconds: 2)).then((_) {
          Navigator.of(context).pop();
          Navigator.of(context).pop();
          Navigator.of(context).push(MaterialPageRoute(
              builder: (BuildContext context) => AddressesPage()));
        });
      } else {
        _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(status, style: TextStyle(color: Colors.red))));
      }
    }
  }

  Widget _buildTextField(
      {@required String labelText,
      @required Function onSaved,
      String initialValue,
      bool isRequired = false}) {
    return TextFormField(
      initialValue: initialValue,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey),
          borderRadius: BorderRadius.circular(10.0),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey),
          borderRadius: BorderRadius.circular(10.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey),
          borderRadius: BorderRadius.circular(10.0),
        ),
        isDense: true,
        filled: true,
        fillColor: Colors.white,
        labelText: labelText,
        labelStyle: TextStyle(color: Colors.grey),
        errorStyle: TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
      ),
      keyboardType: TextInputType.text,
      validator: (String value) {
        if (isRequired) {
          if (value.isEmpty)
            return 'This Field is Required';
          else
            return null;
        } else
          return null;
      },
      onSaved: onSaved,
    );
  }
}
