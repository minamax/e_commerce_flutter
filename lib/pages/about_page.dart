import 'package:e_commerce/items/appbar.dart';
import 'package:e_commerce/network/home_api_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';

import '../main.dart';

class AboutPage extends StatefulWidget {
  @override
  _AboutPageState createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  Map<String, dynamic> about;

  @override
  void initState() {
    super.initState();
    _getAbout();
  }

  void _getAbout() async {
    about = await HomeApiService.instance.fetchAbout();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context, 'About'),
      body: about == null
          ? Center(
              child: CircularProgressIndicator(),
            )
          : SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  SizedBox(height: 10),
                  Padding(
                    padding: EdgeInsets.all(15),
                    child: Center(
                      child: Html(
                        data: currentLocale.languageCode == 'en' ? about['about_en'] : about['about_ar'],
                      ),
                    ),
                  ),
                ],
              ),
            ),
    );
  }
}
