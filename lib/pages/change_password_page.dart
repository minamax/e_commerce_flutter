import 'package:e_commerce/pages/home_page.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';

import '../app_localization.dart';
import '../network/auth_api_service.dart';
import '../items/appbar.dart';
import '../items/text_form_item.dart';
import '../models/model.dart';

class ChangePasswordPage extends StatefulWidget {
  final String apiToken;

  ChangePasswordPage(this.apiToken);

  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePasswordPage> {
  String _newPassword;
  String _oldPassword;
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  ProgressDialog loadingDialog;

  Widget _buildBody() {
    double screenWidth = MediaQuery.of(context).size.width;
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(horizontal: screenWidth * .1),
      child: Column(
        children: <Widget>[
          SizedBox(height: 20),
          Align(
            alignment: AlignmentDirectional.centerStart,
            child: Text(
              AppLocalizations.of(context).translate('Enter your old and new passwords'),
              style: TextStyle(color: Colors.grey),
            ),
          ),
          SizedBox(height: 20),
          CustomTextFormField(
            type: CustomTextFormFieldType.OldPassword,
            onSaved: (String value) {
              _oldPassword = value;
            },
          ),
          SizedBox(height: 20),
          CustomTextFormField(
            type: CustomTextFormFieldType.NewPassword,
            onSaved: (String value) {
              _newPassword = value;
            },
          ),
          SizedBox(height: 20),
          Container(
            width: screenWidth - screenWidth * .2,
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Text(
                AppLocalizations.of(context).translate('Submit'),
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              textColor: Colors.white,
              onPressed: () {
                _submitForm();
              },
            ),
          ),
        ],
      ),
    );
  }

  void _submitForm() async {
    // showDialog(
    //     context: context,
    //     barrierDismissible: false,
    //     builder: (BuildContext context) {
    //       return Container(
    //         width: 50,
    //         height: 50,
    //         child: Dialog(
    //           child: CircularProgressIndicator(),
    //         ),
    //       );
    //     });
    FocusScope.of(context).unfocus();
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      loadingDialog = ProgressDialog(context, ProgressDialogType.Normal);
      loadingDialog.setMessage(AppLocalizations.of(context).translate('Please Wait...'));
      loadingDialog.show();
      String status = await AuthApiService.instance
          .updatePassword(widget.apiToken, _oldPassword, _newPassword);
      loadingDialog.hide();
      // Navigator.pop(context);
      if (status == 'Password Changed Successfully') {
        _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(
          AppLocalizations.of(context).translate(status),
          style: TextStyle(color: Colors.green),
        )));
        Future.delayed(Duration(seconds: 2)).then((_) {
          Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (BuildContext context) => HomePage(4)));
        });
      } else {
        _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(status, style: TextStyle(color: Colors.red))));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: buildAppBar(context, 'Change Password'),
      body: Form(
        key: _formKey,
        child: _buildBody(),
      ),
    );
  }
}
