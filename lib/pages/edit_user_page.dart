import 'dart:io';

import 'package:e_commerce/network/auth_api_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:progress_dialog/progress_dialog.dart';

import '../app_localization.dart';
import '../models/model.dart';
import '../items/text_form_item.dart';
import 'home_page.dart';

class EditUserPage extends StatefulWidget {
  final User user;

  EditUserPage(this.user);

  @override
  _EditUserPageState createState() => _EditUserPageState();
}

class _EditUserPageState extends State<EditUserPage> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController _birthDateCon = TextEditingController();
  File file;
  bool hasImage = false;
  ProgressDialog loadingDialog;
  final Map<String, dynamic> _formData = {
    'api_token': null,
    'name': null,
    'email': null,
    'gender': null,
    'birth_date': null,
    'image': null,
  };
  User user;
  String dropdownValue;

  @override
  void initState() {
    super.initState();
    user = widget.user;
    _formData['api_token'] = user.apiToken;
    _formData['name'] = user.name;
    _formData['email'] = user.email;
    if (user.gender == 'Female')
      dropdownValue = 'Female';
    else if (user.gender == 'Male')
      dropdownValue = 'Male';
    else
      dropdownValue = 'Undefined';
    if (user.birthDate == '0000-00-00' || user.birthDate == null)
      _formData['birth_date'] = '1980-01-01';
    else
      _formData['birth_date'] = user.birthDate;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: _buildAppBar(),
      body: Form(
        key: _formKey,
        child: _buildBody(),
      ),
    );
  }

  Widget _buildAppBar() {
    return AppBar(
      elevation: 0.0,
      leading: IconButton(
        icon: Icon(Icons.done),
        onPressed: () {
          _submitForm();
        },
      ),
      title: Center(
        child: Text(
          AppLocalizations.of(context).translate('Edit Profile'),
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
      ),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.close),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ],
    );
  }

  Widget _buildBody() {
    _birthDateCon.text = _formData['birth_date'];
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            SizedBox(height: 10),
            Stack(
              alignment: Alignment.bottomRight,
              children: <Widget>[
                CircleAvatar(
                  backgroundImage: file == null
                      ? NetworkImage(
                          'https://e-commerce-dev.intcore.net/${user.image}')
                      : FileImage(file),
                  radius: 70,
                ),
                Positioned(
                  left: 100,
                  top: 100,
                  child: IconButton(
                    iconSize: 32,
                    icon: Icon(Icons.camera_enhance),
                    onPressed: () async {
                      file = await ImagePicker.pickImage(
                          source: ImageSource.gallery);
                      if (file != null) {
                        hasImage = true;
                        _formData['image'] = file.path;
                      }
                      setState(() {});
                    },
                  ),
                ),
              ],
            ),
            SizedBox(height: 20),
            CustomTextFormField(
              type: CustomTextFormFieldType.Name,
              initialValue: user.name,
              onSaved: (String value) {
                _formData['name'] = value;
              },
            ),
            SizedBox(height: 20),
            CustomTextFormField(
              type: CustomTextFormFieldType.Email,
              initialValue: user.email,
              onSaved: (String value) {
                _formData['email'] = value;
              },
            ),
            SizedBox(height: 20),
            GestureDetector(
              onTap: () => _selectBirthDate(context),
              child: AbsorbPointer(
                child: TextFormField(
                  controller: _birthDateCon,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    isDense: true,
                    filled: true,
                    fillColor: Colors.white,
                    labelText:
                        AppLocalizations.of(context).translate('Birth Date'),
                    labelStyle: TextStyle(color: Colors.grey),
                    errorStyle: TextStyle(
                        color: Colors.red, fontWeight: FontWeight.bold),
                    prefixIcon: Icon(
                      Icons.calendar_today,
                      color: Colors.grey,
                    ),
                  ),
                  keyboardType: null,
                ),
              ),
            ),
            SizedBox(height: 15),
            Padding(
              padding: EdgeInsets.only(left: 10.0),
              child: Row(
                children: <Widget>[
                  Text(
                    AppLocalizations.of(context).translate('Gender') + ' : ',
                    style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(width: 20),
                  DropdownButton<String>(
                    value: dropdownValue,
                    onChanged: (String newValue) {
                      setState(() {
                        dropdownValue = newValue;
                        _formData['gender'] = dropdownValue;
                      });
                    },
                    items: <String>['Undefined', 'Male', 'Female']
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(AppLocalizations.of(context).translate(value)),
                      );
                    }).toList(),
                  ),
                ],
              ),
            ),
            SizedBox(height: 20),
          ],
        ),
      ),
    );
  }

  void _submitForm() async {
    // showDialog<Null>(
    //     context: context,
    //     barrierDismissible: false,
    //     builder: (BuildContext context) {
    //       return Container(
    //         width: 50,
    //         height: 50,
    //         child: Dialog(
    //           child: Container(
    //             child: CircularProgressIndicator(),
    //           ),
    //         ),
    //       );
    //     });
    FocusScope.of(context).unfocus();
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      loadingDialog = ProgressDialog(context, ProgressDialogType.Normal);
      if (hasImage) {
        loadingDialog.setMessage(AppLocalizations.of(context).translate('Uploading...'));
      } else {
        loadingDialog.setMessage(AppLocalizations.of(context).translate('Please Wait...'));
      }
      loadingDialog.show();
      String status =
          await AuthApiService.instance.updateProfile(_formData, hasImage);
      loadingDialog.hide();
      if (status == 'Profile Updated Successfully') {
        _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(AppLocalizations.of(context).translate(status), style: TextStyle(color: Colors.green))));
        Future.delayed(Duration(seconds: 2)).then((_) {
          Navigator.of(context).pushReplacement(MaterialPageRoute(
              builder: (BuildContext context) => HomePage(4)));
        });
      } else {
        _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(status, style: TextStyle(color: Colors.red))));
      }
    }
  }

  void _selectBirthDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime.parse(_formData['birth_date']),
        firstDate: DateTime(1940),
        lastDate: DateTime(1999));
    if (picked != null)
      setState(() {
        _formData['birth_date'] = convertToDate(picked);
        _birthDateCon.text = _formData['birth_date'];
      });
  }

  String convertToDate(DateTime date) {
    String day, month, year;
    date.day < 10 ? day = '0' + date.day.toString() : day = date.day.toString();
    date.month < 10
        ? month = '0' + date.month.toString()
        : month = date.month.toString();
    year = date.year.toString();
    return '$year-$month-$day';
  }
}
