import 'package:flutter/material.dart';

import '../bloc/bloc.dart';
import '../items/brand_item.dart';
import '../items/appbar.dart';

class BrandsPage extends StatefulWidget {
  @override
  _BrandsPageState createState() => _BrandsPageState();
}

class _BrandsPageState extends State<BrandsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context, 'Brands'),
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    return ListView.builder(
      itemCount: bloc.deals.topBrands.length,
      itemBuilder: (BuildContext context, int index) {
        return BrandItem.build(context, bloc.deals.topBrands, index);
      },
    );
  }
}
