import 'package:e_commerce/app_localization.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:flutter_offline/flutter_offline.dart';

import 'paged_products_page.dart';
import 'side_categories_page.dart';
import '../models/model.dart';
import '../widgets/home_screen_widget.dart';
import '../widgets/deals_screen_widget.dart';
import '../widgets/wish_list_screen_widget.dart';
import '../widgets/cart_screen_widget.dart';
import '../widgets/account_screen_widget.dart';
import '../bloc/bloc.dart';
import '../items/exit_function.dart';

class HomePage extends StatefulWidget {
  final int startingIndex;

  HomePage([this.startingIndex = 0]);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController controller = TextEditingController();
  int currentTabIndex = 0;
  int cartItems = 0;
  bool searchMode = false;
  String query;
  Text screenTitle;
  List<String> titles = [
    'Home',
    'Deals',
    'Wish List',
    'Cart',
    'Account',
  ];
  List<BottomNavigationBarItem> bottomNavigationBarItems = [
    BottomNavigationBarItem(
      title: Text('Home'),
      icon: Icon(Icons.home),
    ),
    BottomNavigationBarItem(
      title: Text('Deals'),
      icon: Icon(Icons.monetization_on),
    ),
    BottomNavigationBarItem(
      title: Text('Wish List'),
      icon: Icon(Icons.favorite),
    ),
    BottomNavigationBarItem(
      title: Text('Cart'),
      icon: StreamBuilder<Cart>(
        stream: bloc.cartStream,
        builder: (BuildContext context, AsyncSnapshot<Cart> snapshot) {
          return snapshot.hasData && snapshot.data.totalItems != 0
              ? Stack(
                  alignment: AlignmentDirectional.topEnd,
                  children: <Widget>[
                    Icon(Icons.shopping_cart),
                    CircleAvatar(
                      backgroundColor: Colors.red,
                      radius: 5,
                    ),
                  ],
                )
              : Stack(
                  alignment: AlignmentDirectional.topEnd,
                  children: <Widget>[
                    Icon(Icons.shopping_cart),
                    CircleAvatar(
                      backgroundColor: Colors.red,
                      radius: 0,
                    ),
                  ],
                );
        },
      ),
    ),
    BottomNavigationBarItem(
      title: Text('Account'),
      icon: Icon(Icons.person),
    ),
  ];
  List<Widget> screens = [
    HomeScreenWidget(),
    DealsScreenWidget(),
    WishListScreenWidget(),
    CartScreenWidget(),
    AccountScreenWidget(),
  ];

  @override
  void initState() {
    super.initState();
    bloc.fetchWishListCart(WishListCartType.Cart);
    currentTabIndex = widget.startingIndex;
    screenTitle = bottomNavigationBarItems[currentTabIndex].title;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => exitApp(context),
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.grey[200],
        bottomNavigationBar: _buildBottomNavigationBar(),
        appBar: _buildAppBar(),
        body: OfflineBuilder(
          connectivityBuilder: (
            BuildContext context,
            ConnectivityResult connectivity,
            Widget child,
          ) {
            return connectivity == ConnectivityResult.none
                ? Center(
                    child: Text(
                      AppLocalizations.of(context).translate('No Internet Access'),
                      style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                  )
                : screens[currentTabIndex];
          },
          child: screens[currentTabIndex],
        ),
      ),
    );
  }

  Widget _buildBottomNavigationBar() {
    return BottomNavigationBar(
      onTap: (int index) {
        setState(() {
          currentTabIndex = index;
          screenTitle = bottomNavigationBarItems[index].title;
        });
      },
      type: BottomNavigationBarType.fixed,
      elevation: 0.0,
      backgroundColor: Colors.white,
      iconSize: 28,
      unselectedItemColor: Colors.grey,
      selectedItemColor: Colors.black,
      selectedFontSize: 0,
      unselectedFontSize: 0,
      showSelectedLabels: false,
      showUnselectedLabels: false,
      currentIndex: currentTabIndex,
      items: bottomNavigationBarItems,
    );
  }

  Widget _buildAppBar() {
    return searchMode == false
        ? AppBar(
            elevation: 0.0,
            centerTitle: true,
            title: Text(
                AppLocalizations.of(context).translate(screenTitle.data),
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
            leading: IconButton(
              icon: Icon(Icons.menu),
              onPressed: () {
                Navigator.push(
                    context,
                    PageTransition(
                        type: PageTransitionType.downToUp,
                        child: SideCategoriesPage()));
              },
            ),
            actions: <Widget>[
              StreamBuilder<Cart>(
                  stream: bloc.cartStream,
                  builder:
                      (BuildContext context, AsyncSnapshot<Cart> snapshot) {
                    return Stack(
                      alignment: AlignmentDirectional.topEnd,
                      children: <Widget>[
                        IconButton(
                          icon: Icon(Icons.shopping_cart),
                          onPressed: () {
                            setState(() {
                              currentTabIndex = 3;
                            });
                          },
                        ),
                        snapshot.hasData && snapshot.data.totalItems != 0
                            ? Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: CircleAvatar(
                                  backgroundColor: Colors.red,
                                  minRadius: 8,
                                  maxRadius: 10,
                                  child: Text(
                                      snapshot.data.totalItems.toString(),
                                      style: TextStyle(
                                          fontSize: 12, color: Colors.white)),
                                ),
                              )
                            : Container(),
                      ],
                    );
                  }),
              IconButton(
                icon: Icon(Icons.search),
                onPressed: () {
                  setState(() {
                    searchMode = true;
                  });
                },
              ),
            ],
          )
        : AppBar(
            elevation: 0,
            backgroundColor: Colors.black,
            leading: IconButton(
              icon: Icon(
                Icons.close,
                color: Colors.white,
              ),
              onPressed: () {
                setState(() {
                  searchMode = false;
                });
              },
            ),
            actions: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width - 50,
                child: TextField(
                  controller: controller,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    isDense: true,
                    filled: true,
                    fillColor: Colors.white,
                  ),
                  onSubmitted: (String value) {
                    query = value;
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (BuildContext context) => PagedProductsPage(
                              PagedProductsType.ByKeyword,
                              keyword: query,
                            ),
                      ),
                    );
                  },
                ),
              ),
            ],
          );
  }
}
