import 'package:flutter/material.dart';

import '../bloc/bloc.dart';
import '../items/category_item.dart';
import '../items/appbar.dart';

class CategoriesPage extends StatefulWidget {
  @override
  _CategoriesPageState createState() => _CategoriesPageState();
}

class _CategoriesPageState extends State<CategoriesPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context, 'Categories'),
      body: _buildBody(),
    );
  }

  Widget _buildBody() {
    return ListView.builder(
      itemCount: bloc.home.topCategories.length,
      itemBuilder: (BuildContext context, int index) {
        return CategoryItem.build(context, bloc.home.topCategories, index);
      },
    );
  }
}
