import 'package:flutter/material.dart';

import '../app_localization.dart';
import '../items/addresses_item.dart';
import '../items/appbar.dart';
import '../models/model.dart';
import '../bloc/bloc.dart';
import 'edit_address_page.dart';

class AddressesPage extends StatefulWidget {
  final bool isChoosing;
  final AsyncSnapshot<AddressesList> snapshot;

  AddressesPage([this.isChoosing = false, this.snapshot]);

  @override
  _AddressesPageState createState() => _AddressesPageState();
}

class _AddressesPageState extends State<AddressesPage> {
  @override
  void initState() {
    super.initState();
    if (!widget.isChoosing) {
      bloc.fetchAddresses();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add_location),
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (BuildContext context) => EditAddressPage()));
        },
      ),
      appBar: buildAppBar(context, 'Addresses'),
      body: widget.isChoosing
          ? widget.snapshot.data.addresses.isNotEmpty
              ? _buildAddresses(widget.snapshot)
              : Center(
                  child: Text(
                    AppLocalizations.of(context).translate('No Addresses'),
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                )
          : StreamBuilder<AddressesList>(
              stream: bloc.addressesStream,
              builder: (BuildContext context,
                  AsyncSnapshot<AddressesList> snapshot) {
                if (snapshot.hasData) {
                  return snapshot.data.addresses.isNotEmpty
                      ? _buildAddresses(snapshot)
                      : Center(
                          child: Text(
                            AppLocalizations.of(context).translate('No Addresses'),
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                        );
                } else {
                  return Center(child: CircularProgressIndicator());
                }
              },
            ),
    );
  }

  Widget _buildAddresses(AsyncSnapshot<AddressesList> snapshot) {
    if (snapshot.data.addresses.length == 0) {
      return Center(
          child: Text(AppLocalizations.of(context).translate('No Addresses'),
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)));
    } else {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: ListView.builder(
              itemCount: snapshot.data.addresses.length,
              itemBuilder: (BuildContext context, int index) {
                return Column(
                  children: <Widget>[
                    Container(
                      color: Colors.grey[200],
                      height: 5,
                    ),
                    widget.isChoosing
                        ? InkWell(
                            child: AddressesItem(snapshot, index),
                            onTap: () {
                              Navigator.of(context).pop(index);
                            },
                          )
                        : AddressesItem(snapshot, index),
                  ],
                );
              },
            ),
          ),
        ],
      );
    }
  }
}
