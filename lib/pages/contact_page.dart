import 'package:e_commerce/items/appbar.dart';
import 'package:e_commerce/items/text_form_item.dart';
import 'package:e_commerce/models/model.dart';
import 'package:e_commerce/network/home_api_service.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';

import '../app_localization.dart';
import 'home_page.dart';

class ContactPage extends StatefulWidget {
  @override
  _ContactPageState createState() => _ContactPageState();
}

class _ContactPageState extends State<ContactPage> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  ProgressDialog loadingDialog;
  Map<String, dynamic> _formData = {
    'name': null,
    'email': null,
    'message': null,
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: buildAppBar(context, 'Contact Us'),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(15),
            child: Column(
              children: <Widget>[
                CustomTextFormField(
                  type: CustomTextFormFieldType.Name,
                  onSaved: (String value) {
                    _formData['name'] = value;
                  },
                ),
                SizedBox(height: 20),
                CustomTextFormField(
                  type: CustomTextFormFieldType.Email,
                  onSaved: (String value) {},
                ),
                SizedBox(height: 20),
                _buildMessageField(),
                SizedBox(height: 20),
                Container(
                  width: MediaQuery.of(context).size.width * .8,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    child: Text(
                      AppLocalizations.of(context).translate('Submit'),
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                    textColor: Colors.white,
                    onPressed: () {
                      _submitForm();
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildMessageField() {
    return TextFormField(
      maxLines: 3,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey),
          borderRadius: BorderRadius.circular(10.0),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey),
          borderRadius: BorderRadius.circular(10.0),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey),
          borderRadius: BorderRadius.circular(10.0),
        ),
        isDense: true,
        filled: true,
        fillColor: Colors.white,
        labelText: AppLocalizations.of(context).translate('Message'),
        labelStyle: TextStyle(color: Colors.grey),
        errorStyle: TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
        prefixIcon: Icon(
          Icons.send,
        ),
      ),
      validator: (String value) {
        if (value.isEmpty)
          return AppLocalizations.of(context).translate('This Field Is Required');
        else
          return null;
      },
      onSaved: (String value) {
        _formData['message'] = value;
      },
    );
  }

  void _submitForm() async {
    // showDialog(
    //     context: context,
    //     barrierDismissible: false,
    //     builder: (BuildContext context) {
    //       return Container(
    //         width: 50,
    //         height: 50,
    //         child: Dialog(
    //           child: CircularProgressIndicator(),
    //         ),
    //       );
    //     });
    FocusScope.of(context).unfocus();
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      loadingDialog = ProgressDialog(context, ProgressDialogType.Normal);
      loadingDialog.setMessage(AppLocalizations.of(context).translate('Please Wait...'));
      loadingDialog.show();
      String status = await HomeApiService.instance.contact(_formData);
      loadingDialog.hide();
      // Navigator.pop(context);
      if (status == 'Message Sent Successfully') {
        _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(
          AppLocalizations.of(context).translate(status),
          style: TextStyle(color: Colors.green),
        )));
        Future.delayed(Duration(seconds: 2)).then((_) {
          Navigator.of(context).pushReplacement(MaterialPageRoute(
              builder: (BuildContext context) => HomePage(4)));
        });
      } else {
        _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(status, style: TextStyle(color: Colors.red))));
      }
    }
  }
}
