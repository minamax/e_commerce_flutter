import 'package:flutter/material.dart';
import 'package:flutter_verification_code_input/flutter_verification_code_input.dart';

import '../app_localization.dart';
import '../services/auth_local_service.dart';
import '../items/appbar.dart';
import 'new_password_page.dart';

class VerificationCodePage extends StatefulWidget {
  final String _phone;

  VerificationCodePage(this._phone);

  @override
  _VerificationCodePageState createState() => _VerificationCodePageState();
}

class _VerificationCodePageState extends State<VerificationCodePage> {
  String _code;
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  Widget _buildBody() {
    double screenWidth = MediaQuery.of(context).size.width;
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(horizontal: screenWidth * .1),
      child: Column(
        children: <Widget>[
          SizedBox(height: 20),
          Align(
            alignment: AlignmentDirectional.centerStart,
            child: Text(
              AppLocalizations.of(context).translate('Enter the verification code sent to your phone'),
              style: TextStyle(color: Colors.grey),
            ),
          ),
          SizedBox(height: 20),
          VerificationCodeInput(
            keyboardType: TextInputType.number,
            length: 4,
            autofocus: true,
            onCompleted: (String input) {
              _code = input;
              _submitForm();
            },
          ),
          SizedBox(height: 20),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              // TextFormField(
              //   buildCounter: (BuildContext context,
              //       {currentLength: 0, maxLength: 1, isFocused: false}) {
              //     return Container();
              //   },
              //   maxLength: 1,
              //   maxLengthEnforced: true,
              //   decoration: InputDecoration(
              //     isDense: true,
              //     filled: true,
              //     fillColor: Colors.white,
              //   ),
              //   keyboardType: TextInputType.number,
              //   validator: (String value) {
              //     if (value.isEmpty || !value.contains(RegExp(r'[0-9]'))) {
              //       return '';
              //     } else {
              //       return null;
              //     }
              //   },
              //   onSaved: (String value) {
              //     digit1 = int.parse(value);
              //   },
              // ),
              // SizedBox(width: 5),
              // TextFormField(
              //   buildCounter: (BuildContext context,
              //       {currentLength: 0, maxLength: 1, isFocused: false}) {
              //     return Container();
              //   },
              //   maxLength: 1,
              //   maxLengthEnforced: true,
              //   decoration: InputDecoration(
              //     isDense: true,
              //     filled: true,
              //     fillColor: Colors.white,
              //   ),
              //   keyboardType: TextInputType.number,
              //   validator: (String value) {
              //     if (value.isEmpty || !value.contains(RegExp(r'[0-9]'))) {
              //       return '';
              //     } else {
              //       return null;
              //     }
              //   },
              //   onSaved: (String value) {
              //     digit2 = int.parse(value);
              //   },
              // ),
              // SizedBox(width: 5),
              // TextFormField(
              //   buildCounter: (BuildContext context,
              //       {currentLength: 0, maxLength: 1, isFocused: false}) {
              //     return Container();
              //   },
              //   maxLength: 1,
              //   maxLengthEnforced: true,
              //   decoration: InputDecoration(
              //     isDense: true,
              //     filled: true,
              //     fillColor: Colors.white,
              //   ),
              //   keyboardType: TextInputType.number,
              //   validator: (String value) {
              //     if (value.isEmpty || !value.contains(RegExp(r'[0-9]'))) {
              //       return '';
              //     } else {
              //       return null;
              //     }
              //   },
              //   onSaved: (String value) {
              //     digit3 = int.parse(value);
              //   },
              // ),
              // SizedBox(width: 5),
              // TextFormField(
              //   buildCounter: (BuildContext context,
              //       {currentLength: 0, maxLength: 1, isFocused: false}) {
              //     return Container();
              //   },
              //   maxLength: 1,
              //   maxLengthEnforced: true,
              //   decoration: InputDecoration(
              //     isDense: true,
              //     filled: true,
              //     fillColor: Colors.white,
              //   ),
              //   keyboardType: TextInputType.number,
              //   validator: (String value) {
              //     if (value.isEmpty || !value.contains(RegExp(r'[0-9]'))) {
              //       return '';
              //     } else {
              //       return null;
              //     }
              //   },
              //   onSaved: (String value) {
              //     digit4 = int.parse(value);
              //   },
              // ),
              // SizedBox(width: 5),
            ],
          ),
          // Center(
          //   child: Container(
          //     alignment: AlignmentDirectional.bottomCenter,
          //     padding: EdgeInsets.only(bottom: 20.0),
          //     width: screenWidth - screenWidth * .2,
          //     child: RaisedButton(
          //       shape: RoundedRectangleBorder(
          //         borderRadius: BorderRadius.circular(10.0),
          //       ),
          //       child: Text(
          //         'Submit',
          //         style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          //       ),
          //       textColor: Colors.white,
          //       onPressed: () {
          //         _submitForm();
          //       },
          //     ),
          //   ),
          // ),
        ],
      ),
    );
  }

  void _submitForm() async {
    // showDialog(
    //     context: context,
    //     barrierDismissible: false,
    //     builder: (BuildContext context) {
    //       return Container(
    //         width: 50,
    //         height: 50,
    //         child: Dialog(
    //           child: CircularProgressIndicator(),
    //         ),
    //       );
    //     });
    FocusScope.of(context).unfocus();
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      bool status = await AuthLocalService.instance.verifyCode(_code);
      if (status) {
        Navigator.of(context).push(MaterialPageRoute(
              builder: (BuildContext context) => NewPasswordPage(widget._phone, _code)));
      } else {
        _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(AppLocalizations.of(context).translate('Code Error'), style: TextStyle(color: Colors.red))));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: buildAppBar(context, 'Verification Code'),
      body: Form(
        key: _formKey,
        child: _buildBody(),
      ),
    );
  }
}
