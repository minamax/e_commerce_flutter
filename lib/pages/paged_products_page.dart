import 'package:e_commerce/pages/product_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:progress_dialog/progress_dialog.dart';

import '../app_localization.dart';
import '../bloc/bloc.dart';
import '../main.dart';
import '../models/model.dart';
import '../items/appbar.dart';

class PagedProductsPage extends StatefulWidget {
  final PagedProductsType pageType;
  final Category category;
  final Brand brand;
  final String keyword;

  PagedProductsPage(this.pageType, {this.category, this.brand, this.keyword});
  @override
  _PagedProductsPageState createState() => _PagedProductsPageState();
}

class _PagedProductsPageState extends State<PagedProductsPage> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  ScrollController _controller = ScrollController();
  Stream<PagedProducts> _stream;
  ProgressDialog loadingDialog;
  String _title;
  bool isLoading = false;
  bool lastPage = false;

  @override
  void initState() {
    super.initState();
    bloc.fetchPagedProducts(widget.pageType,
        category: widget.category,
        brand: widget.brand,
        keyword: widget.keyword);
    switch (widget.pageType) {
      case PagedProductsType.NewArrival:
        _stream = bloc.newArrivalStream;
        _title = 'New Arrival';
        break;
      case PagedProductsType.BestSeller:
        _stream = bloc.bestSellerStream;
        _title = 'Best Seller';
        break;
      case PagedProductsType.HotDeals:
        _stream = bloc.hotDealsStream;
        _title = 'Hot Deals';
        break;
      case PagedProductsType.ByCategory:
        _stream = bloc.categoryStream;
        _title = currentLocale.languageCode == 'en'
            ? widget.category.nameEn
            : widget.category.nameAr;
        break;
      case PagedProductsType.ByBrand:
        _stream = bloc.brandStream;
        _title = currentLocale.languageCode == 'en'
            ? widget.brand.nameEn
            : widget.brand.nameAr;
        break;
      case PagedProductsType.ByKeyword:
        _stream = bloc.searchStream;
        _title = widget.keyword;
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    loadingDialog = ProgressDialog(context, ProgressDialogType.Normal);
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.grey[200],
      appBar: buildAppBar(context, _title),
      body: StreamBuilder<PagedProducts>(
        stream: _stream,
        builder: (BuildContext context, AsyncSnapshot<PagedProducts> snapshot) {
          if (snapshot.hasData) {
            return snapshot.data.products.isNotEmpty
                ? _buildPagedProducts(snapshot)
                : Center(
                    child: Text(
                        AppLocalizations.of(context).translate('No Items'),
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold)));
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }

  Widget _buildPagedProducts(AsyncSnapshot<PagedProducts> snapshot) {
    return NotificationListener<ScrollNotification>(
      onNotification: (ScrollNotification notification) {
        if (!lastPage) {
          _loadNewPage(notification);
        }
      },
      child: CustomScrollView(
        controller: _controller,
        slivers: <Widget>[
          SliverGrid(
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2, childAspectRatio: 0.8),
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                return InkWell(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => ProductPage(snapshot.data.products[index])));
                  },
                  child: Container(
                    height: MediaQuery.of(context).size.height / 2.5,
                    child: Stack(
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Card(
                              color: Colors.white,
                              child: Padding(
                                padding: EdgeInsets.symmetric(vertical: 12.0),
                                child: FadeInImage.assetNetwork(
                                  placeholder: 'assets/loading.gif',
                                  image:
                                      'https://e-commerce-dev.intcore.net/${snapshot.data.products[index].images[0].image}',
                                  fit: BoxFit.scaleDown,
                                  height:
                                      MediaQuery.of(context).size.height / 5,
                                  width: MediaQuery.of(context).size.width / 2,
                                ),
                              ),
                              margin: EdgeInsets.all(10.0),
                            ),
                            Container(
                              width: MediaQuery.of(context).size.width / 2.25,
                              child: Text(
                                currentLocale.languageCode == 'en'
                                    ? snapshot.data.products[index].nameEn
                                    : snapshot.data.products[index].nameAr,
                                overflow: TextOverflow.ellipsis,
                                maxLines: 1,
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(top: 3.0),
                              width: MediaQuery.of(context).size.width / 2.25,
                              child: Text(
                                '\$ ${snapshot.data.products[index].price}',
                                style: TextStyle(fontWeight: FontWeight.bold),
                                overflow: TextOverflow.ellipsis,
                                maxLines: 1,
                              ),
                            ),
                            SizedBox(height: 5),
                          ],
                        ),
                        StreamBuilder<Product>(
                            stream: bloc.productFavoriteStream,
                            builder: (BuildContext context,
                                AsyncSnapshot<Product> productSnapshot) {
                              return Positioned(
                                left: MediaQuery.of(context).size.width / 2 -
                                    60.0,
                                top: 10.0,
                                child: IconButton(
                                  alignment: Alignment.center,
                                  icon: snapshot.data.products[index].isFav
                                      ? Icon(Icons.favorite, color: Colors.red)
                                      : Icon(Icons.favorite_border,
                                          color: Colors.grey),
                                  onPressed: () async {
                                    loadingDialog.setMessage(
                                        AppLocalizations.of(context)
                                            .translate('Please Wait...'));
                                    loadingDialog.show();
                                    String statusMessage =
                                        await bloc.favoriteProduct(
                                            snapshot.data.products[index]);
                                    loadingDialog.hide();
                                    _scaffoldKey.currentState
                                        .removeCurrentSnackBar();
                                    _scaffoldKey.currentState.showSnackBar(
                                        SnackBar(content: Text(statusMessage)));
                                    setState(() {});
                                  },
                                ),
                              );
                            }),
                      ],
                    ),
                  ),
                );
              },
              childCount: snapshot.data.products.length,
            ),
          ),
          SliverToBoxAdapter(
            child: lastPage || snapshot.data.total < 10
                ? Container()
                : Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Center(child: CircularProgressIndicator()),
                  ),
          ),
        ],
      ),
    );
  }

  void _loadNewPage(ScrollNotification notification) async {
    if (notification is ScrollUpdateNotification) {
      if (_controller.position.maxScrollExtent >= _controller.offset &&
          _controller.position.maxScrollExtent - _controller.offset <= 100) {
        if (isLoading == false) {
          isLoading = true;
          print('Loading New Page');
          bool status = await bloc.fetchPagedProductsNextPage(widget.pageType);
          lastPage = !status;
          isLoading = false;
          setState(() {});
        }
      }
    }
  }
}
