import 'package:e_commerce/bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../app_localization.dart';

class LanguagePage extends StatefulWidget {
  @override
  _LanguagePageState createState() => _LanguagePageState();
}

class _LanguagePageState extends State<LanguagePage> {
  String language;
  SharedPreferences prefs;

  void _getLanguage() async {
    prefs = await SharedPreferences.getInstance();
    if (prefs.getString('language') != null) {
      language = prefs.getString('language');
    } else {
      language = 'English';
    }
    setState(() {});
  }

  void _setLanguage() async {
    await bloc.setLocale(language);
    // Navigator.of(context).pushNamedAndRemoveUntil('/', (_) => false);
    Navigator.of(context).pop();
  }

  @override
  void initState() {
    super.initState();
    _getLanguage();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: language == null
          ? Center(
              child: CircularProgressIndicator(),
            )
          : SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  SizedBox(height: 10),
                  RadioListTile<String>(
                    title: Text(
                      AppLocalizations.of(context).translate('English'),
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    value: 'English',
                    groupValue: language,
                    onChanged: (String newValue) {
                      setState(() {
                        language = newValue;
                      });
                    },
                  ),
                  SizedBox(height: 10),
                  RadioListTile<String>(
                    title: Text(
                      AppLocalizations.of(context).translate('Arabic'),
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    value: 'Arabic',
                    groupValue: language,
                    onChanged: (String newValue) {
                      setState(() {
                        language = newValue;
                      });
                    },
                  ),
                ],
              ),
            ),
    );
  }

  Widget _buildAppBar() {
    return AppBar(
      elevation: 0.0,
      leading: IconButton(
        icon: Icon(Icons.done),
        onPressed: () async {
          _setLanguage();
        },
      ),
      title: Center(
        child: Text(
          AppLocalizations.of(context).translate('Select Language'),
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
        ),
      ),
      actions: <Widget>[
        IconButton(
          icon: Icon(Icons.close),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ],
    );
  }
}
