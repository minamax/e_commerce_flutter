import 'package:e_commerce/models/model.dart';
import 'package:flutter/material.dart';

import '../bloc/bloc.dart';
import '../items/side_category_item.dart';
import '../items/avatar_item.dart';

class SideCategoriesPage extends StatefulWidget {
  @override
  _SideCategoriesPageState createState() => _SideCategoriesPageState();
}

class _SideCategoriesPageState extends State<SideCategoriesPage> {
  @override
  void initState() {
    super.initState();
    if (bloc.home == null) {
      bloc.fetchHome();
    }
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 1,
      child: Scaffold(
        appBar: _buildAppBar(),
        body: _buildBody(),
      ),
    );
  }

  Widget _buildBody() {
    return Column(
      children: <Widget>[
        AvatarItem(),
        StreamBuilder<Home>(
          stream: bloc.homeStream,
          builder: (BuildContext context, AsyncSnapshot<Home> snapshot) {
            return bloc.home != null
                ? Expanded(
                    child: ListView.builder(
                      itemCount: bloc.home.topCategories.length,
                      itemBuilder: (BuildContext context, int index) {
                        return SideCategoryItem.build(
                            context, bloc.home.topCategories, index);
                      },
                    ),
                  )
                : Container(
                  height: MediaQuery.of(context).size.height / 2,
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  );
          },
        ),
      ],
    );
  }

  Widget _buildAppBar() {
    return AppBar(
      elevation: 0.0,
      backgroundColor: Colors.white,
      leading: IconButton(
        icon: Icon(Icons.close),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
    );
  }
}
