import 'dart:async';

import 'package:e_commerce/main.dart';
import 'package:e_commerce/network/favorite_api_service.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';

import '../app_localization.dart';
import '../items/appbar.dart';
import '../models/model.dart';
import '../bloc/bloc.dart';

class ProductPage extends StatefulWidget {
  final Product product;

  ProductPage(this.product);

  @override
  _ProductPageState createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  ProgressDialog loadingDialog;
  int imageIndex = 0;
  String statusMessage = '';
  int chosenRating = 0;
  int rating = 0;
  String review = 'I recommend this product';
  bool isLoading = false;
  bool justLoaded = false;

  @override
  Widget build(BuildContext context) {
    loadingDialog = ProgressDialog(context, ProgressDialogType.Normal);
    return Scaffold(
      key: _scaffoldKey,
      appBar: buildAppBar(
          context,
          currentLocale.languageCode == 'en'
              ? widget.product.nameEn
              : widget.product.nameAr),
      body: _buildBody(),
      bottomNavigationBar: _buildMessageButton(),
    );
  }

  Widget _buildMessageButton() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 60,
      child: justLoaded == false
          ? RaisedButton(
              color: Colors.black,
              child: !isLoading
                  ? Text(
                      AppLocalizations.of(context).translate('ADD TO CART'),
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    )
                  : Center(
                      child: ListTile(
                        contentPadding: EdgeInsets.only(left: 100),
                        leading: CircularProgressIndicator(
                          valueColor:
                              AlwaysStoppedAnimation<Color>(Colors.white),
                        ),
                        title: Text(
                          AppLocalizations.of(context).translate('Adding...'),
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              color: Colors.white),
                        ),
                      ),
                    ),
              textColor: Colors.white,
              onPressed: () async {
                setState(() {
                  isLoading = true;
                });
                statusMessage = await bloc.addToCart(widget.product);
                setState(() {
                  isLoading = false;
                  justLoaded = true;
                  Timer(Duration(seconds: 2), () {
                    setState(() {
                      justLoaded = false;
                    });
                  });
                });
              },
            )
          : RaisedButton(
              color: statusMessage == 'Item Added Successfully'
                  ? Colors.green
                  : Colors.red,
              child: Text(
                AppLocalizations.of(context).translate(statusMessage),
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              textColor: Colors.white,
              onPressed: () {},
            ),
    );
  }

  Widget _buildBody() {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Stack(
            alignment: Alignment.topRight,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                child: FadeInImage.assetNetwork(
                  placeholder: 'assets/loading.gif',
                  image:
                      'https://e-commerce-dev.intcore.net/${widget.product.images[imageIndex].image}',
                  fit: BoxFit.cover,
                  width: MediaQuery.of(context).size.width - 20,
                  height: MediaQuery.of(context).size.width - 20,
                ),
              ),
              StreamBuilder<Product>(
                  stream: bloc.productFavoriteStream,
                  builder: (BuildContext context,
                      AsyncSnapshot<Product> productSnapshot) {
                    return Positioned(
                      right: 20,
                      top: 20.0,
                      child: IconButton(
                        alignment: Alignment.center,
                        iconSize: 40,
                        icon: widget.product.isFav
                            ? Icon(Icons.favorite, color: Colors.red)
                            : Icon(Icons.favorite_border, color: Colors.grey),
                        onPressed: () async {
                          loadingDialog.setMessage(AppLocalizations.of(context)
                              .translate('Please Wait...'));
                          loadingDialog.show();
                          String statusMessage =
                              await bloc.favoriteProduct(widget.product);
                          loadingDialog.hide();
                          _scaffoldKey.currentState.removeCurrentSnackBar();
                          _scaffoldKey.currentState.showSnackBar(SnackBar(
                              content: Text(AppLocalizations.of(context)
                                  .translate(statusMessage))));
                        },
                      ),
                    );
                  }),
            ],
          ),
          widget.product.images.length > 1
              ? Container(
                  padding: EdgeInsets.only(top: 10),
                  alignment: AlignmentDirectional.center,
                  height: MediaQuery.of(context).size.height / 6,
                  width: MediaQuery.of(context).size.width - 20,
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: widget.product.images.length,
                    itemBuilder: (BuildContext context, int index) {
                      return InkWell(
                        onTap: () {
                          setState(() {
                            imageIndex = index;
                          });
                        },
                        child: Card(
                          child: FadeInImage.assetNetwork(
                            placeholder: 'assets/loading.gif',
                            image:
                                'https://e-commerce-dev.intcore.net/${widget.product.images[index].image}',
                            fit: BoxFit.cover,
                            width: MediaQuery.of(context).size.width / 4,
                            height: MediaQuery.of(context).size.width / 4,
                          ),
                        ),
                      );
                    },
                  ),
                )
              : Container(),
          Container(
            alignment: AlignmentDirectional.topStart,
            padding: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
            child: Stack(
              overflow: Overflow.visible,
              alignment: AlignmentDirectional.centerStart,
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: Text(
                    currentLocale.languageCode == 'en'
                        ? widget.product.nameEn
                        : widget.product.nameAr,
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    maxLines: 2,
                  ),
                ),
                Positioned.directional(
                  textDirection: currentLocale.languageCode == 'en'
                      ? TextDirection.ltr
                      : TextDirection.rtl,
                  top: 30,
                  end: 10,
                  child: Text(
                    '\$ ${widget.product.price.toString()}',
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
          ),
          Container(
            alignment: Alignment.topLeft,
            height: 30,
            width: MediaQuery.of(context).size.width - 20,
            padding: EdgeInsets.only(top: 10),
            child: _buildStars(false),
          ),
          Container(
            alignment: Alignment.topLeft,
            width: MediaQuery.of(context).size.width - 20,
            padding: EdgeInsets.only(top: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: 5),
                Text(
                  AppLocalizations.of(context).translate('Description'),
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 3),
                Text(currentLocale.languageCode == 'en'
                    ? widget.product.descriptionEn
                    : widget.product.descriptionAr),
                SizedBox(height: 5),
                widget.product.subCategoryId != null
                    ? Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            AppLocalizations.of(context)
                                    .translate('Subcategory') +
                                ' : ',
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                          Text(currentLocale.languageCode == 'en'
                              ? widget.product.subcategory.nameEn
                              : widget.product.subcategory.nameAr),
                        ],
                      )
                    : Container(),
                InkWell(
                  onTap: () {
                    _showDialog();
                  },
                  child: Container(
                    height: 40,
                    width: MediaQuery.of(context).size.width - 40,
                    alignment: AlignmentDirectional.centerEnd,
                    child: Text(
                      AppLocalizations.of(context)
                          .translate('Rate This Product'),
                      style: TextStyle(
                          decoration: TextDecoration.underline,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 15),
        ],
      ),
    );
  }

  Widget _buildStars(bool isClickable) {
    if (isClickable)
      return ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: 5,
        itemBuilder: (BuildContext context, int index) {
          if (index <= chosenRating) {
            return IconButton(
              icon: Icon(Icons.star, color: Colors.orange),
              iconSize: 36,
              onPressed: () {
                chosenRating = index;
                Navigator.pop(context);
                _showDialog();
              },
            );
          } else {
            return IconButton(
              icon: Icon(Icons.star_border, color: Colors.orange),
              iconSize: 36,
              onPressed: () {
                chosenRating = index;
                Navigator.pop(context);
                _showDialog();
              },
            );
          }
        },
      );
    else
      return ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: 5,
        itemBuilder: (BuildContext context, int index) {
          if (index < widget.product.totalRate) {
            return Icon(Icons.star, color: Colors.orange);
          } else {
            return Icon(Icons.star_border, color: Colors.orange);
          }
        },
      );
  }

  Future<Null> _showDialog() {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(5),
                height: 80,
                child: _buildStars(true),
              ),
              Container(
                padding: EdgeInsets.all(5),
                child: TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                  ),
                  initialValue: currentLocale.languageCode == 'en'
                      ? 'I recommend this product'
                      : ' أنا اقترح هذا المنتج',
                  autovalidate: true,
                  validator: (String value) {
                    if (value.isEmpty)
                      return AppLocalizations.of(context)
                          .translate('This Field Is Required');
                    else
                      return null;
                  },
                  onFieldSubmitted: (String value) {
                    review = value;
                  },
                ),
              ),
              RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Text(
                  AppLocalizations.of(context).translate('Rate'),
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                ),
                textColor: Colors.white,
                onPressed: () async {
                  Navigator.pop(context);
                  loadingDialog.setMessage(
                      AppLocalizations.of(context).translate('Please Wait...'));
                  loadingDialog.show();
                  String statusMessage = await FavoriteApiService.instance
                      .rateProduct(widget.product, review, rating);
                  loadingDialog.hide();
                  _scaffoldKey.currentState.removeCurrentSnackBar();
                  _scaffoldKey.currentState
                      .showSnackBar(SnackBar(content: Text(statusMessage)));
                },
              ),
            ],
          ),
        );
      },
    );
  }
}
