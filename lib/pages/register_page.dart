import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';

import '../app_localization.dart';
import '../models/model.dart';
import '../network/auth_api_service.dart';
import '../items/exit_function.dart';
import '../items/text_form_item.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final Map<String, dynamic> _formData = {
    'name': null,
    'phone': null,
    'email': null,
    'password': null
  };
  ProgressDialog loadingDialog;

  Widget _buildAppBar() {
    return AppBar(
      elevation: 0.0,
      automaticallyImplyLeading: false,
      actions: <Widget>[
        FlatButton(
          color: Colors.white,
          child: Text(AppLocalizations.of(context).translate('Skip'),
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold)),
          onPressed: () {
            Navigator.pushNamed(context, '/home');
          },
        ),
      ],
    );
  }

  Widget _buildBody() {
    double screenWidth = MediaQuery.of(context).size.width;
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(horizontal: screenWidth * .1),
      child: Column(
        children: <Widget>[
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              AppLocalizations.of(context).translate('Signup'),
              style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(height: 20),
          CustomTextFormField(
            type: CustomTextFormFieldType.Name,
            onSaved: (String value) {
              _formData['name'] = value;
            },
          ),
          SizedBox(height: 20.0),
          CustomTextFormField(
            type: CustomTextFormFieldType.Phone,
            onSaved: (String value) {
              _formData['phone'] = value;
            },
          ),
          SizedBox(height: 15.0),
          CustomTextFormField(
            type: CustomTextFormFieldType.Email,
            onSaved: (String value) {
              _formData['email'] = value;
            },
          ),
          SizedBox(height: 20.0),
          CustomTextFormField(
            type: CustomTextFormFieldType.Password,
            onSaved: (String value) {
              _formData['password'] = value;
            },
          ),
          SizedBox(height: 20),
          Container(
            width: screenWidth - screenWidth * .2,
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Text(
                AppLocalizations.of(context).translate('Create Account'),
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              textColor: Colors.white,
              onPressed: () {
                _submitForm();
              },
            ),
          ),
          FlatButton(
            child: Text(AppLocalizations.of(context).translate('Already Have an Account? Login')),
            textColor: Colors.black,
            onPressed: () {
              return Navigator.pushReplacementNamed(context, '/login');
            },
          ),
        ],
      ),
    );
  }

  void _submitForm() async {
    // showDialog(
    //     context: context,
    //     barrierDismissible: false,
    //     builder: (BuildContext context) {
    //       return Container(
    //         width: 50,
    //         height: 50,
    //         child: Dialog(
    //           child: CircularProgressIndicator(),
    //         ),
    //       );
    //     });
    FocusScope.of(context).unfocus();
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      loadingDialog = ProgressDialog(context, ProgressDialogType.Normal);
      loadingDialog.setMessage(AppLocalizations.of(context).translate('Please Wait...'));
      loadingDialog.show();
      String status = await AuthApiService.instance.registerUser(_formData);
      // Navigator.pop(context);
      loadingDialog.hide();
      if (status == 'ok') {
        Navigator.pushReplacementNamed(context, '/home');
      } else {
        _scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text(status, style: TextStyle(color: Colors.red))));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => exitApp(context),
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        appBar: _buildAppBar(),
        body: Form(
          key: _formKey,
          child: _buildBody(),
        ),
      ),
    );
  }
}
