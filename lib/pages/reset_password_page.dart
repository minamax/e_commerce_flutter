import 'package:e_commerce/items/text_form_item.dart';
import 'package:e_commerce/models/model.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';

import '../app_localization.dart';
import '../network/auth_api_service.dart';
import '../items/appbar.dart';
import 'verification_code_page.dart';

class ResetPasswordPage extends StatefulWidget {
  @override
  _ResetPasswordState createState() => _ResetPasswordState();
}

class _ResetPasswordState extends State<ResetPasswordPage> {
  String _phone;
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  ProgressDialog loadingDialog;

  Widget _buildBody() {
    double screenWidth = MediaQuery.of(context).size.width;
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(horizontal: screenWidth * .1),
      child: Column(
        children: <Widget>[
          SizedBox(height: 20),
          Container(
            alignment: AlignmentDirectional.centerStart,
            child: Text(
              AppLocalizations.of(context).translate('Enter the phone number you used to register'),
              style: TextStyle(color: Colors.grey),
            ),
          ),
          SizedBox(height: 20),
          CustomTextFormField(
            type: CustomTextFormFieldType.Phone,
            onSaved: (String value) {
              _phone = value;
            },
          ),
          SizedBox(height: 20),
          Container(
            width: screenWidth - screenWidth * .2,
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Text(
                AppLocalizations.of(context).translate('Send Reset SMS'),
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              textColor: Colors.white,
              onPressed: () {
                _submitForm();
              },
            ),
          ),
        ],
      ),
    );
  }

  void _submitForm() async {
    // showDialog(
    //     context: context,
    //     barrierDismissible: false,
    //     builder: (BuildContext context) {
    //       return Container(
    //         width: 50,
    //         height: 50,
    //         child: Dialog(
    //           child: CircularProgressIndicator(),
    //         ),
    //       );
    //     });
    FocusScope.of(context).unfocus();
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      loadingDialog = ProgressDialog(context, ProgressDialogType.Normal);
      loadingDialog.setMessage(AppLocalizations.of(context).translate('Please Wait...'));
      loadingDialog.show();
      String code = await AuthApiService.instance.resetEmail(_phone);
      loadingDialog.hide();
      if (!code.contains(' ')) {
        _scaffoldKey.currentState.hideCurrentSnackBar();
        _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(code)));
        Future.delayed(Duration(seconds: 2)).then((_) {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (BuildContext context) => VerificationCodePage(_phone)));
        });
      } else {
        _scaffoldKey.currentState.showSnackBar(
            SnackBar(content: Text(code, style: TextStyle(color: Colors.red))));
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      appBar: buildAppBar(context, 'Forgot Password'),
      body: Form(
        key: _formKey,
        child: _buildBody(),
      ),
    );
  }
}
