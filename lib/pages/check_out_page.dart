import 'dart:io';
import 'dart:math';

import 'package:e_commerce/main.dart';
import 'package:e_commerce/network/payment_api_service.dart';
import 'package:e_commerce/pages/orders_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:progress_dialog/progress_dialog.dart';

import '../app_localization.dart';
import '../models/model.dart';
import '../bloc/bloc.dart';
import '../items/addresses_item.dart';
import '../items/order_with_message_item.dart';
import '../items/appbar.dart';
import '../widgets/cart_screen_widget.dart';
import 'addresses_page.dart';

class CheckOutPage extends StatefulWidget {
  final AsyncSnapshot<Cart> snapshot;

  CheckOutPage(this.snapshot);

  @override
  _CheckOutPageState createState() => _CheckOutPageState();
}

class _CheckOutPageState extends State<CheckOutPage> {
  static const platform = const MethodChannel('com.minamax.e_commerce/payment');
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  ProgressDialog loadingDialog;
  String orderId = 'ORD-' + Random().nextInt(10000).toString();
  String deviceId = '';
  String sdkToken = '';
  int paymentAmount = 0;
  int addressId;
  int addressIndex = 0;
  int stepIndex = 0;
  int radioIndex = 0;
  int paymentType = 0;
  List<bool> stepActive = [true, false, false, false];

  @override
  void initState() {
    super.initState();
    bloc.fetchAddresses();
    bloc.fetchUser();
    _getDeviceId();
    paymentAmount = (widget.snapshot.data.totalPrice +
        int.parse(widget.snapshot.data.shipping));
  }

  @override
  Widget build(BuildContext context) {
    loadingDialog = ProgressDialog(context, ProgressDialogType.Normal);
    return Scaffold(
      appBar: buildAppBar(context, 'Check Out'),
      body: StreamBuilder<AddressesList>(
        stream: bloc.addressesStream,
        builder: (BuildContext context, AsyncSnapshot<AddressesList> snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            addressId = snapshot.data.addresses.first.id;
            return Theme(
              data: ThemeData(
                buttonColor: Colors.white,
                accentColor: Colors.black,
                primaryColor: Colors.black,
              ),
              child: Stepper(
                currentStep: stepIndex,
                type: StepperType.horizontal,
                controlsBuilder: (BuildContext context,
                    {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
                  switch (stepIndex) {
                    case 0:
                      return Container(
                        padding: EdgeInsets.only(top: 20),
                        height: 70,
                        child: RaisedButton(
                          padding: EdgeInsets.all(15),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: Text(
                            AppLocalizations.of(context)
                                .translate('PROCEED TO PAYMENT'),
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                          textColor: Colors.white,
                          color: Colors.black,
                          onPressed: onStepContinue,
                        ),
                      );
                      break;
                    case 1:
                      return Container(
                        padding: EdgeInsets.only(top: 20),
                        height: 70,
                        child: RaisedButton(
                          padding: EdgeInsets.all(15),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: Text(
                            AppLocalizations.of(context).translate('BACK'),
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                          textColor: Colors.black,
                          color: Colors.white,
                          onPressed: onStepCancel,
                        ),
                      );
                      break;
                    case 2:
                      return Row(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(top: 20),
                            width: MediaQuery.of(context).size.width / 2 - 35,
                            height: 70,
                            child: RaisedButton(
                              padding: EdgeInsets.all(15),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              child: Text(
                                AppLocalizations.of(context)
                                    .translate('CONFIRM'),
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold),
                              ),
                              textColor: Colors.white,
                              color: Colors.black,
                              onPressed: onStepContinue,
                            ),
                          ),
                          SizedBox(width: 20),
                          Container(
                            padding: EdgeInsets.only(top: 20),
                            width: MediaQuery.of(context).size.width / 2 - 35,
                            height: 70,
                            child: RaisedButton(
                              padding: EdgeInsets.all(15),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              child: Text(
                                AppLocalizations.of(context).translate('BACK'),
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold),
                              ),
                              textColor: Colors.black,
                              color: Colors.white,
                              onPressed: onStepCancel,
                            ),
                          ),
                        ],
                      );
                      break;
                    case 3:
                      return Container();
                      break;
                  }
                },
                onStepContinue: () {
                  if (stepIndex == 2) {
                    if (paymentType == 2) {
                      bloc.createOrder(addressId, 2);
                      setState(() {
                        stepIndex++;
                        stepActive[stepIndex] = true;
                      });
                    } else if (paymentType == 1) {
                      if (Platform.isAndroid) {
                        _requestToken();
                      } else {
                        _scaffoldKey.currentState.removeCurrentSnackBar();
                        _scaffoldKey.currentState.showSnackBar(
                            SnackBar(content: Text(AppLocalizations.of(context).translate('Not Supported On Your Device'))));
                      }
                    }
                  } else {
                    setState(() {
                      stepIndex++;
                      stepActive[stepIndex] = true;
                    });
                  }
                },
                onStepCancel: () {
                  setState(() {
                    stepActive[stepIndex] = false;
                    stepIndex--;
                  });
                },
                steps: [
                  Step(
                    isActive: stepActive[0],
                    title: Text(AppLocalizations.of(context).translate('Ship')),
                    content: Column(
                      children: <Widget>[
                        Container(
                          child: AddressesItem(snapshot, addressIndex),
                        ),
                        Container(
                          height: 2,
                          color: Colors.transparent,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          color: Colors.white,
                          child: FlatButton(
                            child: Text(
                              AppLocalizations.of(context)
                                  .translate('Change Address'),
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            onPressed: () async {
                              int result = await Navigator.of(context).push(
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          AddressesPage(true, snapshot)));
                              if (result != null) {
                                addressIndex = result;
                                addressId =
                                    snapshot.data.addresses[addressIndex].id;
                              }
                            },
                          ),
                        ),
                        Container(
                          height: 20,
                          color: Colors.transparent,
                        ),
                        Container(
                          height: widget.snapshot.data.items.length * 65.0,
                          color: Colors.white,
                          child: ListView.builder(
                            physics: const NeverScrollableScrollPhysics(),
                            itemCount: widget.snapshot.data.items.length,
                            itemBuilder: (BuildContext context, int index) {
                              return Container(
                                color: Colors.white,
                                child: ListTile(
                                  leading: Text(
                                    AppLocalizations.of(context)
                                            .translate('Qty') +
                                        ' : ' +
                                        widget
                                            .snapshot.data.items[index].quantity
                                            .toString(),
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                  title: Text(
                                    currentLocale.languageCode == 'en'
                                        ? widget.snapshot.data.items[index]
                                            .product.nameEn
                                        : widget.snapshot.data.items[index]
                                            .product.nameAr,
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                  trailing: Text(
                                    '\$ ' +
                                        widget.snapshot.data.items[index]
                                            .product.price
                                            .toString(),
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                        Container(
                          height: 20,
                          color: Colors.transparent,
                        ),
                        Container(
                          color: Colors.white,
                          padding: currentLocale.languageCode == 'en'
                              ? EdgeInsets.only(left: 20)
                              : EdgeInsets.only(right: 20),
                          child: CartScreenWidgetState()
                              .buildSubTotal(context, widget.snapshot),
                        ),
                      ],
                    ),
                  ),
                  Step(
                    isActive: stepActive[1],
                    title: Text(AppLocalizations.of(context).translate('Pay')),
                    content: Column(
                      children: <Widget>[
                        InkWell(
                          onTap: () {
                            setState(() {
                              paymentType = 1;
                              stepIndex++;
                              stepActive[stepIndex] = true;
                            });
                          },
                          child: Container(
                            color: Colors.white,
                            child: ListTile(
                              leading: Icon(Icons.credit_card),
                              title: Text(
                                AppLocalizations.of(context)
                                    .translate('Credit Card'),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              trailing: Icon(Icons.arrow_forward_ios),
                            ),
                          ),
                        ),
                        Container(
                          height: 20,
                          color: Colors.transparent,
                        ),
                        InkWell(
                          onTap: () {
                            setState(() {
                              paymentType = 2;
                              stepIndex++;
                              stepActive[stepIndex] = true;
                            });
                          },
                          child: Container(
                            color: Colors.white,
                            child: ListTile(
                              leading: Icon(Icons.attach_money),
                              title: Text(
                                AppLocalizations.of(context)
                                    .translate('Cash On Delivery'),
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              trailing: Icon(Icons.arrow_forward_ios),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Step(
                    isActive: stepActive[2],
                    title:
                        Text(AppLocalizations.of(context).translate('Confirm')),
                    content: Column(
                      children: <Widget>[
                        Container(
                          color: Colors.white,
                          child: Column(
                            children: <Widget>[
                              ListTile(
                                title: Text(
                                  AppLocalizations.of(context)
                                          .translate('Payment Method') +
                                      ' : ',
                                  style: TextStyle(fontSize: 14),
                                ),
                                subtitle: Center(
                                  child: Text(
                                    paymentType == 2
                                        ? AppLocalizations.of(context)
                                            .translate('Cash On Delivery')
                                        : AppLocalizations.of(context)
                                            .translate('Credit Card'),
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black),
                                  ),
                                ),
                              ),
                              Container(
                                color: Colors.white,
                                padding: currentLocale.languageCode == 'en'
                                    ? EdgeInsets.only(left: 20)
                                    : EdgeInsets.only(right: 20),
                                child: CartScreenWidgetState()
                                    .buildSubTotal(context, widget.snapshot),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Step(
                    isActive: stepActive[3],
                    title: Text(AppLocalizations.of(context).translate('Done')),
                    content: StreamBuilder<OrderWithMessage>(
                      stream: bloc.createdOrderStream,
                      builder: (BuildContext context,
                          AsyncSnapshot<OrderWithMessage> orderSnapshot) {
                        if (!orderSnapshot.hasData) {
                          return Center(
                            child: CircularProgressIndicator(),
                          );
                        } else {
                          return Column(
                            children: <Widget>[
                              Container(
                                child: Column(
                                  children: <Widget>[
                                    Center(
                                      child: Stack(
                                        alignment: Alignment.center,
                                        children: <Widget>[
                                          CircleAvatar(
                                            backgroundColor: Colors.black,
                                            radius: 40,
                                          ),
                                          Icon(
                                            Icons.done_all,
                                            size: 40,
                                            color: Colors.white,
                                          ),
                                        ],
                                      ),
                                    ),
                                    Container(
                                      color: Colors.transparent,
                                      height: 10,
                                    ),
                                    Text(
                                      AppLocalizations.of(context)
                                          .translate('Done'),
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 24,
                                      ),
                                    ),
                                    Container(
                                      color: Colors.transparent,
                                      height: 10,
                                    ),
                                    OrderWithMessageItem.build(
                                      context,
                                      orderSnapshot,
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(top: 20),
                                      width: MediaQuery.of(context).size.width -
                                          80,
                                      height: 70,
                                      child: RaisedButton(
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                        ),
                                        child: Text(
                                          AppLocalizations.of(context)
                                              .translate('Track Your Order'),
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        textColor: Colors.black,
                                        color: Colors.white,
                                        onPressed: () {
                                          Navigator.pushReplacement(
                                              context,
                                              MaterialPageRoute(
                                                  builder:
                                                      (BuildContext context) =>
                                                          OrdersPage()));
                                        },
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(top: 20),
                                      width: MediaQuery.of(context).size.width -
                                          80,
                                      height: 70,
                                      child: RaisedButton(
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                        ),
                                        child: Text(
                                          AppLocalizations.of(context)
                                              .translate('Return To Home'),
                                          style: TextStyle(
                                              fontSize: 16,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        textColor: Colors.white,
                                        color: Colors.black,
                                        onPressed: () {
                                          Navigator.pushNamedAndRemoveUntil(
                                              context, '/home', (_) => false);
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          );
                        }
                      },
                    ),
                  ),
                ],
              ),
            );
          }
        },
      ),
    );
  }

  void _getDeviceId() async {
    String result = await platform.invokeMethod('getDeviceId');
    print(result);
    if (result != null) {
      deviceId = result;
    }
  }

  void _requestToken() async {
    loadingDialog
        .setMessage(AppLocalizations.of(context).translate('Please Wait...'));
    loadingDialog.show();
    dynamic responseData =
        await PaymentApiService.instance.requestToken(deviceId);
    if (responseData['response_message'] == 'Success') {
      sdkToken = responseData['sdk_token'];
      await _authorizePayment();
    }
  }

  Future<void> _authorizePayment() async {
    Navigator.pop(context);
    String amount = paymentAmount.toStringAsFixed(2);
    amount = amount.replaceAll('.', '');
    String response = await platform.invokeMethod('showPaymentPage', {
      'command': 'PURCHASE',
      'sdk_token': sdkToken,
      'customer_email': "minamax1994@gmail.com",
      'currency': 'SAR',
      'amount': amount,
      'language': currentLocale.languageCode,
      'merchant_reference': orderId,
    });
    if (response == "Payment Succeessful") {
      bloc.createOrder(addressId, 2);
      setState(() {
        stepIndex++;
        stepActive[stepIndex] = true;
      });
    }
  }
}
