import 'package:flutter/material.dart';

import '../app_localization.dart';
import '../bloc/bloc.dart';
import '../models/model.dart';
import '../items/orders_item.dart';
import '../items/appbar.dart';

class OrdersPage extends StatefulWidget {
  @override
  _OrdersPageState createState() => _OrdersPageState();
}

class _OrdersPageState extends State<OrdersPage> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    bloc.fetchOrders();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.grey[200],
      appBar: buildAppBar(context, 'Orders'),
      body: StreamBuilder<OrdersList>(
        stream: bloc.ordersStream,
        builder: (BuildContext context, AsyncSnapshot<OrdersList> snapshot) {
          if (snapshot.hasData) {
            return snapshot.data.orders.isNotEmpty
                ? _buildOrders(snapshot)
                : Center(
                    child: Text(
                      AppLocalizations.of(context).translate('No Orders'),
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                  );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }

  Widget _buildOrders(AsyncSnapshot<OrdersList> snapshot) {
    if (snapshot.data.orders.length == 0) {
      return Center(
          child: Text(AppLocalizations.of(context).translate('No Orders'),
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)));
    } else {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: ListView.builder(
              itemCount: snapshot.data.orders.length,
              itemBuilder: (BuildContext context, int index) {
                return OrdersItem.build(context, snapshot, index);
              },
            ),
          ),
        ],
      );
    }
  }
}
