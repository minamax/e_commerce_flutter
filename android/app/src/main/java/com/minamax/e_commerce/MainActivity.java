package com.minamax.e_commerce;

import android.content.Intent;
import android.os.Bundle;
import io.flutter.app.FlutterActivity;
import io.flutter.plugins.GeneratedPluginRegistrant;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

import com.payfort.fort.android.sdk.base.FortSdk;
import com.payfort.fort.android.sdk.base.callbacks.FortCallBackManager;
import com.payfort.fort.android.sdk.base.callbacks.FortCallback;
import com.payfort.sdk.android.dependancies.base.FortInterfaces;
import com.payfort.sdk.android.dependancies.models.FortRequest;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends FlutterActivity {
    private static final String CHANNEL = "com.minamax.e_commerce/payment";
    public FortCallBackManager fortCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GeneratedPluginRegistrant.registerWith(this);
        new MethodChannel(getFlutterView(), CHANNEL).setMethodCallHandler(
                new MethodCallHandler() {
                    @Override
                    public void onMethodCall(MethodCall call, Result result) {
                        if (call.method.equals("getDeviceId")) {
                            String deviceId = FortSdk.getDeviceId(MainActivity.this);
                            result.success(deviceId);
                        } else if (call.method.equals("showPaymentPage")) {
                            fortCallback = FortCallback.Factory.create();
                            HashMap<String, String> parameters = new HashMap<>();
                            parameters.put("command", "PURCHASE");
                            parameters.put("merchant_reference", call.argument("merchant_reference"));
                            parameters.put("amount", call.argument("amount"));
                            parameters.put("currency", call.argument("currency"));
                            parameters.put("language", call.argument("language"));
                            parameters.put("customer_email", call.argument("customer_email"));
                            parameters.put("sdk_token", call.argument("sdk_token"));
                            FortRequest fortRequest = new FortRequest();
                            fortRequest.setRequestMap(parameters);
                            fortRequest.setShowResponsePage(true);
                            try {
                                FortSdk.getInstance().registerCallback(MainActivity.this, fortRequest, FortSdk.ENVIRONMENT.TEST, 5,
                                        fortCallback, true, new FortInterfaces.OnTnxProcessed() {
                                            @Override
                                            public void onCancel(Map<String, String> requestParamsMap, Map<String,
                                                    String> responseMap) {
                                                result.error("Payment Canceled",null, null);
                                            }

                                            @Override
                                            public void onFailure(Map<String, String> requestParamsMap, Map<String,
                                                    String> fortResponseMap) {
                                                result.error("Payment Failed",null, null);
                                            }

                                            @Override
                                            public void onSuccess(Map<String, String> requestParamsMap, Map<String,
                                                    String> fortResponseMap) {
                                                result.success("Payment Succeessful");
                                            }

                                        });
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            result.notImplemented();
                        }
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(fortCallback != null){
            fortCallback.onActivityResult(requestCode, resultCode, data);
        }
    }
}